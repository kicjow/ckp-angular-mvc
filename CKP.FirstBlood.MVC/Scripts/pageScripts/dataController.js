﻿app.controller("dataController", ["$scope", "$http", function ($scope, $http)
{
    $scope.osoba = {};
    $scope.specjalizacjaArray = {};
    $scope.typykursowArray = {};
    $scope.kursy = [];
    $scope.aplikacje = {};
    $scope.dziedzinaArray = {};
    $scope.specjalizacjaArrayStore = {};

    var zepsuloSieFunction = function (data, code)
    {
        if (code === 401)
        {
            new InfoDialog("Uwaga", "Sesja wygała nastąpi wylogowanie", true, "panel-warning", function () { window.location.replace("../"); }).show();
        }
        else
        {
            new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
        }
    }


    $http.post("../App/RodzajSpecjalnosci", {}).success(function (data)
    {
        $scope.dziedzinaArray = data;
    }).error(zepsuloSieFunction);

    $http.post("../App/TypyKursow", {}).success(function (data)
    {
        $scope.typykursowArray = data;
    }).error(zepsuloSieFunction);

    $http.post("../App/Specjalizacje", {}).success(function (data)
    {
        $scope.specjalizacjaArray = data;
        $scope.specjalizacjaArrayStore = data;
    }).error(zepsuloSieFunction);

    $http.post("../App/Kursy", {}).success(function (data)
    {
        $scope.kursy = data;
    }).error(zepsuloSieFunction);
    $scope.$on("daneOsoboweRefreshed", function (e, osoba)
    {
        $scope.osoba = new DaneOsobowe();
        $scope.osoba.CopyFrom(osoba);
    });
    function RefreshAplikacje(callback, func, arg)
    {
        $http.post("../App/Aplikacje", {}).success(function (data)
        {
            var temp = [];
            for (var i = 0; i < data.Items.length; i++)
            {
                temp.push(new Aplikacja().CopyFrom(data.Items[i]));
            }
            $scope.aplikacje = temp;
            if (func && arg) //&& typeof callback === 'function'
            {
                func(arg);
            }
            else if (func)
            {
                func();
            }

        }).error(zepsuloSieFunction);


    }
    $scope.$on("aplikacjeNeedsRefresh", RefreshAplikacje);

    RefreshAplikacje();

}]);
