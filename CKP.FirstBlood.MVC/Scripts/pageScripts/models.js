﻿var QuestionDialog = (function ()
{
    function QuestionDialog(title, content, isModel, panelClass, callback)
    {
        this.id = "idg" + new Date().getMilliseconds();
        this.dialogResult = false;

        var formElement = $(document.createElement("div"));
        formElement.addClass("infoDialog panel " + (angular.isDefined(panelClass) ? panelClass : "panel-info"));

        if (angular.isDefined(callback))
        {
            this.callback = callback;
        }
        if (angular.isDefined(title))
        {
            formElement.append("<div class='panel-heading'><h3 class='panel-title'>" + title + "</h3></div>");
        }
        formElement.append("<div class='panel-body'>" + content + "<br /><br /><div class='btn btn-success col-lg-3 col-lg-3 col-md-3 col-sm-3 col-xs-3 col-lg-offset-6 col-md-offset-6 col-sm-offset-6 col-xs-offset-6' onclick='$(\"#" + this.id + "\").data(\"dialogInfo\").yes()'>Tak</div>" +
            "<div class='btn btn-primary col-lg-3 col-lg-3 col-md-3 col-sm-3 col-xs-3' onclick='$(\"#" + this.id + "\").data(\"dialogInfo\").no()'>Nie</div></div>");

        this.dialog = formElement;
        if (angular.isDefined(isModel) && isModel)
        {
            this.dialog = $(document.createElement("div"));
            this.dialog.append("<div class='modalElement'></div>", formElement);
        }
        this.dialog.attr("id", this.id);
    }
    QuestionDialog.prototype.close = function ()
    {
        $("#" + this.id).remove();
        (this.callback || angular.noop)(this.dialogResult);
    };
    QuestionDialog.prototype.show = function ()
    {
        this.dialog.appendTo($("body")).data("dialogInfo", this);
        return this;
    };
    QuestionDialog.prototype.yes = function ()
    {
        this.dialogResult = true;
        this.close();
    };
    QuestionDialog.prototype.no = function ()
    {
        this.dialogResult = false;
        this.close();
    };
    return QuestionDialog;
})();

var InfoDialog = (function ()
{
    function InfoDialog(title, content, isModel, panelClass, onClose)
    {
        this.id = "idg" + new Date().getMilliseconds();
        var formElement = $(document.createElement("div"));
        formElement.addClass("infoDialog panel " + (angular.isDefined(panelClass) ? panelClass : "panel-info"));

        if (angular.isDefined(onClose))
        {
            this.onClose = onClose;
        }
        if (angular.isDefined(title))
        {
            formElement.append("<div class='panel-heading'><h3 class='panel-title'>" + title + "</h3></div>");
        }
        formElement.append("<div class='panel-body'>" + content + "<br /><br /><div class='alginRightForBig fullWidthForSmall width300ForBig btn btn-primary' onclick='$(\"#" + this.id + "\").data(\"dialogInfo\").close()'>Zamknij</div></div>");

        this.dialog = formElement;
        if (angular.isDefined(isModel) && isModel)
        {
            this.dialog = $(document.createElement("div"));
            this.dialog.append("<div class='modalElement'></div>", formElement);
        }
        this.dialog.attr("id", this.id);
    }
    InfoDialog.prototype.close = function ()
    {
        $("#" + this.id).remove();
        (this.onClose || angular.noop)();
    };
    InfoDialog.prototype.show = function ()
    {
        this.dialog.appendTo($("body")).data("dialogInfo", this);
    };
    return InfoDialog;
})();


var Faktura = (function ()
{
    var Faktura = function (id, nazwa, kod, miejscowosc, ulica, nip)
    {
        this.Kod = kod;
        this.Id = id;
        this.Miejscowosc = miejscowosc;
        this.Ulica = ulica;
        this.Nip = nip;
        this.Nazwa = nazwa;
        this.WatchDisabled = false;
    }
    return Faktura;
})();

var DaneOsobowe = (function ()
{
    function DaneOsobowe()
    {
        this.Imie = "";
        this.Imie2 = "";
        this.Nazwisko = "";
        this.Miejscowosc = "";
        this.Ulica = "";
        this.KodPocztowy = "";
        this.Poczta = "";
        this.Obywatelstwo = "";
        this.Telefon = "";
        this.Email = "";
        this.Specjalizacja = "";
        this.DataRozpoczecia = "";
        this.DataZakonczenia = "";
        this.NumerPrawa = "";
        this.Faktury = {};
    }
    DaneOsobowe.prototype.CopyFrom = function (other)
    {
        for (var prop in other)
        {
            if (typeof other[prop] !== "function")
            {
                this[prop] = other[prop];
            }
        }
    }
    return DaneOsobowe;
})();



var Aplikacja = (function ()
{
    function Aplikacja()
    {
        this.DaneOsobowe = {};
        this.Status = 0;
        this.DataRozpoczecia = "";
        this.DataZakonczenia = "";
        this.KursNazwa = "";
        this.CzyRodzajKursuDoFaktury = true;
        this.RodzajAplikacjiNazwa = "";
        this.RodzajAplikacjiKod = "";
        this.IDKursu = "";
        this.Faktura = {};
        this.OtwartaEdycja = false;
    }

    //function isDate(val)
    //{
    //    var d = new Date(val);
    //    return !isNaN(d.valueOf());
    //}



    Aplikacja.prototype.CopyFrom = function (other)
    {
        for (var prop in other)
        {
            if (prop === "DaneOsobowe")
            {
                this.DaneOsobowe = new DaneOsobowe();
                this.DaneOsobowe.CopyFrom(other.DaneOsobowe);
                continue;
            }
            if (prop === "Faktura")
            {
                this.Faktura = new Faktura("", other.Faktura.Nazwa, other.Faktura.Kod, other.Faktura.Miejscowosc, other.Faktura.Ulica, other.Faktura.Nip);
                this.Faktura.WatchDisabled = true;
                continue;
            }
            if (typeof other[prop] !== "function")
            {

                function isDate(value) // lifesaving function
                {
                    var dateFormat;
                    if (toString.call(value) === '[object Date]')
                    {
                        return true;
                    }
                    if (typeof value.replace === 'function')
                    {
                        value.replace(/^\s+|\s+$/gm, '');
                    }
                    dateFormat = /(^\d{1,4}[\.|\\/|-]\d{1,2}[\.|\\/|-]\d{1,4})(\s*(?:0?[1-9]:[0-5]|1(?=[012])\d:[0-5])\d\s*[ap]m)?$/;
                    return dateFormat.test(value);
                }


                var czyJestData = isDate(other[prop]);

                if (czyJestData)
                {
                    var parsedDate = new Date(other[prop]);

                    var years = parsedDate.getFullYear();
                    var months = parsedDate.getMonth() + 1;
                    if (months < 10)
                    {
                        months = "0" + months;
                    }
                    var days = parsedDate.getDate();
                    if (days < 10)
                    {
                        days = "0" + days;
                    }
                    var dateString = days + "-" + months + "-" + years;
                    this[prop] = dateString;
                }
                else
                {
                    this[prop] = other[prop];
                }

            }
        }
        return this;
    }

    return Aplikacja;
})();