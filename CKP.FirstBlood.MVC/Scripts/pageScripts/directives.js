﻿var app = angular.module("CKPApp", ["ngAnimate"]);
var rootScope = null;
app.run(['$rootScope', function ($rootScope)
{
    rootScope = $rootScope;
}]);

$('a[data-toggle="tab"]').on('hide.bs.tab', function (e)
{
    var target = $(e.relatedTarget);
    if (e.currentTarget.hash === "#aplikacaEditTab" && $("#aplikacjaEditForm").data('$formController').$dirty && (angular.isUndefined(target.data("forceRedirect")) || !(target.data("forceRedirect"))))
    {
        e.preventDefault();
        var redirectCallback = function (dialogResult)
        {
            if (dialogResult)
            {
                target.data("forceRedirect", true)
                target.tab("show");
                rootScope.$$childHead.$emit("aplikacjeNeedsRefresh");
            }
        }
        new QuestionDialog("Uwaga", "Wykryto niezapisane zmiany. Czy jesteś pewien że chcesz zamknąć edycję aplikacji?", true, "panel-warning", redirectCallback).show();
    }
    target.data("forceRedirect", false);
})

app.directive("datepicker", function ()
{
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel)
        {
            $(element).datepicker({ format: 'dd-mm-yyyy' });
            var unregister = scope.$watch(function ()
            {
                return ngModel.$modelValue;
            }, changed);

            function changed(newValue, oldValue)
            {
                if (oldValue === "" && newValue !== "")
                {
                    $(element).data("datepicker").update(newValue);
                    unregister();
                }
            }
        }
    }
});
