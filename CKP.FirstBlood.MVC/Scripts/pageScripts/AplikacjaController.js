﻿
app.controller("AplikacjeController", ["$scope", "$http", function ($scope, $http)
{
    $scope.specjalizacja = "";
    $scope.nazwa = "";
    $scope.typkursow = "";
    $scope.dziedzinakursow;
    $scope.Status = "";
    // $scope.aplikacjaStatusReadable = "";
    $scope.aplikacjaStatusClass = function (appId)
    {
        if (typeof appId === 'undefined')
        {
            return false;
        }
        var app;
        for (var i = 0; i < $scope.$parent.aplikacje.length; i++)
        {
            if ($scope.$parent.aplikacje[i].IDKursu === appId)
            {
                app = $scope.$parent.aplikacje[i];
            }
        }

        if (typeof app === 'undefined')
        {
            return false;
        }

        var aplikacjaGotowa = app.Status === 1 ? true : false;
        return aplikacjaGotowa ? info() : warning();

        function info()
        {
            app.aplikacjaStatusReadable = "poprawnie.";
            return 'alert-info';
        }

        function warning()
        {
            app.aplikacjaStatusReadable = "niepoprawnie. Proszę uzupełnić brakujące pola.";
            return 'alert-danger';
        }
    }

    $scope.$watch("dziedzinakursow", function ()
    {
        var dziedzina = $scope.dziedzinakursow;
        if (dziedzina !== undefined && dziedzina !== null && dziedzina !== "" && dziedzina.toLowerCase() !== "wybierz") // chryste
        {

            $scope.$parent.specjalizacjaArray = {};

            $scope.$parent.specjalizacjaArray[""] = $scope.$parent.specjalizacjaArrayStore[""];

            var szukany;
            switch (dziedzina)
            {
                case "spc_ldent":
                    szukany = "- Lekarsko-dentystyczne"
                    break;
                case "spc_lek":
                    szukany = "- Lekarskie";
                    break;
                default:
            }

            for (var item in $scope.$parent.specjalizacjaArrayStore)
            {
                if ($scope.$parent.specjalizacjaArrayStore[item].indexOf(szukany) !== -1)
                {
                    $scope.$parent.specjalizacjaArray[item] = $scope.$parent.specjalizacjaArrayStore[item];
                }
            }
            // $scope.$parent.specjalizacjaArray
        }
        else
        {
            $scope.$parent.specjalizacjaArray = $scope.$parent.specjalizacjaArrayStore;
        }
    });

    $scope.isVisible = function (id)
    {
        var kurs = $scope.$parent.kursy[id];
        return (typeof kurs !== "undefined") &&
            ($scope.specjalizacja === "" || kurs.Specjalizacja === $scope.specjalizacja) &&
            ($scope.nazwa === "" || kurs.Nazwa.indexOf($scope.nazwa.toLowerCase()) > -1) &&
            ($scope.typkursow === "" || kurs.Typ === $scope.typkursow) &&
            ($scope.dziedzinakursow === "" || kurs.RodzajSpecjalnosci === $scope.dziedzinakursow);
    }


    $scope.maSpecjalizacje = function (id)
    {
        for (var i = 0; i < $scope.$parent.aplikacje.length; i++)
        {
            if ($scope.$parent.aplikacje[i].IDKursu === id)
            {
                return true;
            }
        }
        return false;
    }
    $scope.pokazAplikacje = function (id)
    {
        // console.log('pokaz aplikacje ' + id);
        if (typeof id !== "string" && typeof id.app !== "undefined")
        {
            id = id.app.IDKursu;
        }

        var aplikacja = null;
        var iPos = 0;
        for (var i = 0; i < $scope.$parent.aplikacje.length; i++)
        {
            if ($scope.$parent.aplikacje[i].IDKursu === id)
            {
                aplikacja = $scope.$parent.aplikacje[i];
                iPos = i;
                break;
            }
        }
        if (aplikacja === null)
        {
            new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
        }
        $("#aplikacjaEditForm").data('$formController').$setPristine();
        $scope.$emit("aplikacjaEdit", aplikacja);
    }
    $scope.dodajAplikacje = function (id)
    {
        var aplikacja = new Aplikacja();
        aplikacja.DaneOsobowe = new DaneOsobowe();
        aplikacja.DaneOsobowe.CopyFrom($scope.$parent.osoba);
        aplikacja.IDKursu = id;
        aplikacja.Status = 0;
        var model = angular.toJson(aplikacja, false);
        $http.post("../App/DodajAplikacje", { "model": model }).success(function (data)
        {
            var funkcjaDoPrzekazania = $scope.pokazAplikacje;
            $scope.$emit("aplikacjeNeedsRefresh", funkcjaDoPrzekazania, aplikacja.IDKursu); //jako argument daje funkcje np. pokaz aplikacje z odpowidniem ID do parametru tej funkcji ktorą przekazuje.
            //$scope.$parent.RefreshAplikacje();function.call();
            //$scope.pokazAplikacje(aplikacja.IDKursu);

        }).error(function (data, code)
        {
            if (code === 401)
            {
                new InfoDialog("Uwaga", "Sesja wygała nastąpi wylogowanie", true, "panel-warning", function () { window.location.re - place("../"); }).show();
            }
            else
            {
                new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
            }
        });

    }
    $scope.usunAplikacje = function (id)
    {
        if (typeof id !== "string" && typeof id.app !== "undefined")
        {
            id = id.app.IDKursu;
        }

        var aplikacja = null;
        var iPos = 0;
        for (var i = 0; i < $scope.$parent.aplikacje.length; i++)
        {
            if ($scope.$parent.aplikacje[i].IDKursu === id)
            {
                aplikacja = $scope.$parent.aplikacje[i];
                iPos = i;
                break;
            }
        }
        if (aplikacja === null)
        {
            new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
        }

        var redirectCallback = function (dialogResult)
        {
            if (dialogResult)
            {
                //   aplikacja.Status = 0; // TODO WAZNE ja wiem, przepraszam, ale nie widze by w aplikacji gdziekolwiek ten status był uzywany a Chrome zamienia go na 1-1-2000 a IE na poprawną wartość 0. na razie tak to ustawilem
                var model = angular.toJson(aplikacja, false);
                $http.post("../App/UsunAplikacje", { "model": model }).success(function (data)
                {
                    $scope.$emit("aplikacjeNeedsRefresh");
                }).error(function (data, code)
                {
                    if (code === 401)
                    {
                        new InfoDialog("Uwaga", "Sesja wygała nastąpi wylogowanie", true, "panel-warning", function () { window.location.replace("../"); }).show();
                    }
                    else
                    {
                        new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
                    }
                });
            }
        };
        new QuestionDialog("Decyzja", "Czy napewno chcesz usunąć wybraną aplikację?", true, "panel-warning", redirectCallback).show();
    }
}]);