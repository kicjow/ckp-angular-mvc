﻿app.controller("daneOsoboweController", ["$scope", "$http", function ($scope, $http)
{
    $scope.osoba = new DaneOsobowe();

    function reloadDaneOsobowe()
    {
        $http.post("../App/DaneOsobowe", {}).success(function (data)
        {
            $scope.osoba.CopyFrom(data);
            $scope.$emit("daneOsoboweRefreshed", $scope.osoba);
        }).error(function (data, code)
        {
            if (code === 401)
            {
                new InfoDialog("Uwaga", "Sesja wygała nastąpi wylogowanie", true, "panel-warning", function () { window.location.replace("../"); }).show();
            }
            else
            {
                new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
            }
        });
    }
    $scope.saveDaneOsobowe = function ()
    {
        var model = angular.toJson($scope.osoba, false);
        $.post("../App/SaveDaneOsobowe", { "model": model }).success(function (data)
        {
            if (data === "1")
            {
                new InfoDialog("Sukces", "Zapis zakończony powodzeniem", true, "panel-success").show();
                reloadDaneOsobowe();
            }
            else
            {
                new InfoDialog("Bład", "Zapis się nie udał", true, "panel-danger").show();
            }
        }).error(function (data, code)
        {
            if (code === 401)
            {
                new InfoDialog("Uwaga", "Sesja wygała nastąpi wylogowanie", true, "panel-warning", function () { window.location.replace("../"); }).show();
            }
            else
            {
                new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
            }
        });
    }

    reloadDaneOsobowe();
}]);