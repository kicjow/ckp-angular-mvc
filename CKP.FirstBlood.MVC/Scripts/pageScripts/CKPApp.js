﻿var app = angular.module("CKPApp", ["ngAnimate", "ngSanitize"]);

app.run(['$rootScope', function ($rootScope)
{

}]);


app.controller("retriveControler", ["$scope", "$http", function ($scope, $http)
{
    $scope.login = "";
    $scope.email = "";

    $scope.retrivePassword = function (e)
    {
        e.preventDefault();

        $http.post("../Account/Retrive", { "login": $scope.login, "email": $scope.email })
        .success(function (data)
        {
            var x = $("#confimation");
            x.show();
        })
        .error(function ()
        {
            $("#loginError").show();
        });

    }
}]);


app.controller("changePasswordControler", ["$scope", function ($scope)
{
    $scope.login = "";
    $scope.password = "";
    $scope.repeatPassword = "";
    $scope.aaaa = function (a)
    {

    }
    //czy cos jeszcze?

}]);


app.controller("loginCotroller", ["$scope", function ($scope)
{
    $scope.login = "";
    $scope.password = "";
    $scope.loginError = false;
    $scope.doLogIn = function (e)
    {
        $("body").append("<div id='overlay' style='background-color:grey;position:fixed;top:0;left:0;height:100%;width:100%;z-index:999;opacity: 0.4;filter: alpha(opacity=40);'><img src='../Content/Images/ajax-loader.gif' class='centeredImage' /></div>");
        e.preventDefault();
        $("#loginError").hide();

        $.ajax({
            url: "../Account/Login",
            type: "POST",
            async: true,
            data: { login: $scope.login, password: $scope.password },
            error: function ()
            {
                $("#loginError").show();
            },
            success: function (data)
            {
                if (data === "1") //TODO poprawic
                {
                    window.location = '../App';
                    return;
                }
                $("#loginError").show();
            },

        }).always(function ()
        {
            $("#overlay").remove();
        });
    }
    $scope.retrivePassword = function ()
    {
        $('#retrivePassExecutor').tab('show');
    }
}]);

app.directive("peselvalidation", function ()
{
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel)
        {
            scope.$watch(function ()
            {
                ngModel.$setValidity("lenght", !scope.brakPesel ? ngModel.$viewValue.length === 11 : true);
                ngModel.$setValidity("required", !scope.brakPesel ? $.trim(ngModel.$viewValue) !== "" : true);
                ngModel.$setValidity("plec", (!scope.brakPesel && scope.plec !== "") ? ngModel.$viewValue[9] !== undefined && (scope.plec === "plec_k" ? ngModel.$viewValue[9] % 2 === 0 : (scope.plec === "plec_m" ? ngModel.$viewValue[9] % 2 === 1 : true)) : true);
                var dataUrodzenia = scope.dataUrodzenia.substr(8, 2) + scope.dataUrodzenia.substr(3, 2) + scope.dataUrodzenia.substr(0, 2);
                ngModel.$setValidity("dataUrodzenia", !scope.brakPesel ? ngModel.$viewValue.substr(0, 6) === dataUrodzenia : true);

                var pesel = ngModel.$viewValue;
                var kontrolna = Number(pesel[0]) + 3 * Number(pesel[1]) + 7 * Number(pesel[2]) + 9 * Number(pesel[3]) + Number(pesel[4]) + 3 * Number(pesel[5]) + 7 * Number(pesel[6]) + 9 * Number(pesel[7]) + Number(pesel[8]) + 3 * Number(pesel[9]) + Number(pesel[10]);
                ngModel.$setValidity("kontrolna", !scope.brakPesel ? kontrolna % 10 === 0 : true);
            });
        }
    }
}).directive("datepicker", function ()
{
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel)
        {
            var unregister = scope.$watch(function ()
            {
                return ngModel.$modelValue;
            }, initialize);

            function initialize(value)
            {
                unregister();
                $(element).val(value).datepicker({ format: 'dd-mm-yyyy' });
            }
        }
    }
}).directive("passwordvalidation", function ()
{
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel)
        {
            scope.$watch(function ()
            {
                ngModel.$setValidity("samePassword", scope.password === ngModel.$viewValue);
            });
        }
    }
});

app.controller("registerController", ["$scope", "$http", function ($scope, $http)
{
    $scope.brakPesel = false;
    $scope.pesel = "";
    $scope.imie = "";
    $scope.nazwisko = "";
    $scope.password = "";
    $scope.dataUrodzenia = "";
    $scope.passwordRepeat = "";
    $scope.email = "";
    $scope.dokumentNumer = "";
    $scope.dokumentRodzaj = "";
    $scope.obywatelstwo = "";
    $scope.obywatelstwoArray = {};

    $http.post("../Home/Obywatelstwa", {}).success(function (data)
    {
        $scope.obywatelstwoArray = data;
    }).error(function ()
    {
        new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
    });

    $scope.rodzajDokumentuArray = {
        "": "Wybierz",
        "dok_do": "Dowód osobisty",
        "dok_pas": "Paszport"
    };
    $scope.plec = "";
    $scope.plecArray = {
        "": "Wybierz",
        "plec_k": "Kobieta",
        "plec_m": "Mężczyzna"
    };
    $scope.serverValidation = "";

    $scope.doRegister = function (e)
    {
        e.preventDefault();
        $("body").append("<div id='overlay' style='background-color:grey;position:fixed;top:0;left:0;height:100%;width:100%;z-index:999;opacity: 0.4;filter: alpha(opacity=40);'><img src='../Content/Images/ajax-loader.gif' class='centeredImage' /></div>");
        $.ajax({
            url: "../Account/Register",
            type: "POST",
            async: true,
            data: $(e.target).serialize(),
            error: function ()
            {
            },
            statusCode: {
                200: function (data)
                {
                    $scope.serverValidation = data;
                },
                201: function (data)
                {
                    window.location = '../App';
                }
            }

        }).always(function ()
        {
            $("#overlay").remove();
        });
    }
}]);