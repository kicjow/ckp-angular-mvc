﻿app.controller("AplikacjaEditController", ["$scope", "$http", function ($scope, $http)
{

    $scope.aplikacja = {};
    $scope.aplikacjaStatusReadable = "";
    // $scope.aplikacjaStatusClass = false;

    $scope.aplikacjaStatusClass = czyGotowa();

    function czyGotowa()
    {
        var app;
        for (var i = 0; i < $scope.$parent.aplikacje.length; i++)
        {
            if ($scope.$parent.aplikacje[i].IDKursu === $scope.aplikacja.IDKursu)
            {
                app = $scope.$parent.aplikacje[i];
            }
        }

        if (typeof app === 'undefined')
        {
            return false;
        }

        var aplikacjaGotowa = app.Status === 1 ? true : false;
        return aplikacjaGotowa ? info() : warning();

        function info()
        {
            $scope.aplikacjaStatusReadable = "poprawnie.";
            return 'alert-info';
        }

        function warning()
        {
            $scope.aplikacjaStatusReadable = "niepoprawnie. Proszę uzupełnić brakujące pola.";
            return 'alert-danger';
        }
    }

    $scope.$watch("aplikacjaStatusClass", czyGotowa());



    $scope.zapiszAplikacje = function ()
    {
        var model = angular.toJson($scope.aplikacja);
        $http.post("../App/ZapiszAplikacje", { "model": model })
            .success(function (data)
            {
                $scope.$emit("aplikacjeNeedsRefresh");//, czyGotowa, $scope.aplikacja.IDKursu);
                $("#aplikacjaEditExecutor").tab("show");
                new InfoDialog("Sukces", "Zapisano zmiany", true, "panel-success").show();
                czyGotowa();

            }).error(function (data, code)
            {
                if (code === 401)
                {
                    new InfoDialog("Uwaga", "Sesja wygała nastąpi wylogowanie", true, "panel-warning", function () { window.location.replace("../"); }).show();
                }
                else
                {
                    new InfoDialog("Błąd", "Błąd serwera aplikacja uruchomi się ponownie", true, "panel-danger", function () { window.location.replace("../"); }).show();
                }
            });

        
    };
    $scope.$parent.$on("aplikacjaEdit", function (e, app)
    {
        $scope.aplikacja = app;

        $scope.aplikacjaStatusClass = function ()
        {
            var app;
            for (var i = 0; i < $scope.$parent.aplikacje.length; i++)
            {
                if ($scope.$parent.aplikacje[i].IDKursu === $scope.aplikacja.IDKursu)
                {
                    app = $scope.$parent.aplikacje[i];
                }
            }

            if (typeof app === 'undefined')
            {
                return false;
            }

            var aplikacjaGotowa = app.Status === 1 ? true : false;
            return aplikacjaGotowa ? info() : warning();

            function info()
            {
                $scope.aplikacjaStatusReadable = "poprawnie.";
                return 'alert-info';
            }

            function warning()
            {
                $scope.aplikacjaStatusReadable = "niepoprawnie. Proszę uzupełnić brakujące pola.";
                return 'alert-danger';
            }
        }

        $("#aplikacjaEditExecutor").tab("show");
    })

    $scope.wczytajFakture = function ()
    {
        $("#fakturyDialog").show();
    }
    $scope.wybierzFakture = function ()
    {
        $scope.aplikacja.Faktura.Kod = $scope.$parent.osoba.Faktury[$scope.fakturaNazwa].Kod;
        $scope.aplikacja.Faktura.Miejscowosc = $scope.$parent.osoba.Faktury[$scope.fakturaNazwa].Miejscowosc;
        $scope.aplikacja.Faktura.Ulica = $scope.$parent.osoba.Faktury[$scope.fakturaNazwa].Ulica;
        $scope.aplikacja.Faktura.Nip = $scope.$parent.osoba.Faktury[$scope.fakturaNazwa].Nip;
        $("#aplikacjaEditForm").data('$formController').$setDirty();
        $("#fakturyDialog").hide();
        $scope.fakturaNazwa = "";
    }
    $scope.anulujWyborFaktury = function ()
    {
        $("#fakturyDialog").hide();
    }
    $scope.fakturaNazwa = "";

    $scope.PokazFakture = function ()
    {
        var boolean;
        if ($scope.aplikacja.CzyRodzajKursuDoFaktury !== undefined)
        {
            boolean = $scope.aplikacja.CzyRodzajKursuDoFaktury;
        }
        else
        {
            boolean = true;
        }

        if (boolean && boolean !== null && boolean !== undefined) // wiem, ale z jakis powodów, ktorych nie udalo mi sie ustalic, czasem boolean byl nullem
        {
            return true;
        }
        return false;
    };
}])
