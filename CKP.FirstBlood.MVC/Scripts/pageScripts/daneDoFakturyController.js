﻿
app.controller("daneDoFakturyController", ["$scope", "$http", function ($scope)
{
    $scope.fakturaNazwa = "";

    $scope.$parent.$on("daneOsoboweRefreshed", function ()
    {
        $scope.fakturaNazwa = "";
    })
    $scope.nowaFaktura = function ()
    {
        var data = new Date();
        var id = 'n' + data.getHours().toString() + data.getMinutes() + data.getSeconds() + data.getMilliseconds();
        $scope.fakturaNazwa = id;
        $scope.$parent.osoba.Faktury[id] = new Faktura(id, "Nowa(" + id + ")");
    }
    $scope.usunFakture = function ()
    {
        if ($scope.fakturaNazwa !== "")
        {
            var current = $scope.fakturaNazwa;
            delete $scope.$parent.osoba.Faktury[current];
            $scope.fakturaNazwa = "";
        }
    }
}]);
