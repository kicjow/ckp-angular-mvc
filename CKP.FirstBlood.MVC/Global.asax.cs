﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using CKP.FirstBlood.MVC.Models;
using Castle.MicroKernel.Registration;
using CKP.FirstBlood.Module.BusinessObjects;
using DevExpress.Xpo;

namespace CKP.FirstBlood.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitLogic();
            //ModelBinders.Binders.Add(typeof(IJsonModel), new JsonModelBinder());
            ModelBinderProviders.BinderProviders.Add(new JsonModelBinder());
            ModelBinderProviders.BinderProviders.Add(new DateTimeModelBinder());
        }

        private void InitLogic()
        {
            var connStr = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

            XPObjectSpaceProvider osProvider = new XPObjectSpaceProvider(connStr, null, true);

            foreach (var type in typeof(XPCoreObject).Assembly.GetTypes())
            {
                if (type.IsSubclassOf(typeof(XPBaseObject)))
                {
                    osProvider.TypesInfo.RegisterEntity(type);
                }
            }

            Logic.Container.Register(Component.For<IObjectSpaceProvider>().Instance(osProvider).LifestyleSingleton());
            Logic.Container.Register(Component.For<IDBProvider>().ImplementedBy<DBProviderImpl>().LifestylePerWebRequest());
            Logic.Container.Register(Component.For<IMyRequest>().ImplementedBy<MyRequest>().LifestylePerWebRequest());
            
        }
    }
}
