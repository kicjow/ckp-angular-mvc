﻿using CKP.FirstBlood.Module.BusinessObjects;
using CKP.FirstBlood.Module.BusinessObjects.EmailProcessor;
using CKP.FirstBlood.MVC.Models;
using CKP.FirstBlood.MVC.Models.Helpers;
using CKP.FirstBlood.MVC.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CKP.FirstBlood.MVC.Controllers
{
    public class AccountController : SuperController
    {
        // GET: Account
        [HttpPost]
        public ActionResult Login(string login, string password)
        {
            AppLogic logic = new AppLogic();
            if (!logic.LoginUser(login, password))
            {
                return Content("0");
            }
            return Content("1");
        }

        [HttpPost]
        public ActionResult Register()
        {


            var formData = Request.Form;
            List<string> errors = new List<string>();

            AppLogic logic = new AppLogic();
            logic.ValidateRegisterForm(formData, errors);
            if (errors.Count != 0)
            {
                return RegistrationError(errors);
            }
            logic.RegisterNewAccount(formData);
            logic.Save();
            if (!logic.LoginUser((string.IsNullOrEmpty(formData["pesel"]) ? formData["dokumentNumer"] : formData["pesel"]), formData["password"]))
            {
                errors.Add("Nieoczekiwany błąd spróbuj jeszcze raz");
                return RegistrationError(errors);
            }

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.Created);
        }

        private ActionResult RegistrationError(List<string> errors)
        {
            return Content(string.Join("<br />", errors));
        }

        [HttpPost]
        public ActionResult Retrive(string login, string email)
        {
            AppLogic logic = new AppLogic();
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            var daneResetu = os.CreateObject<EmailResetDataContainer>();
            daneResetu.ClientEmail = email;
            daneResetu.ClientLogin = login;
            daneResetu.ClientIP = Request.UserHostAddress;
            daneResetu.RequestCreationDateTime = DateTime.Now;
            daneResetu.IsUsed = false;

            os.CommitChanges();

            var linkDOzmiany = string.Format("http://localhost:55589/Account/ChangePassword?id={0}", daneResetu.Oid);
            var emailSettings = new EmailSetting() { MeilToAdress = email }; // zainicjalizuj odpowiednie dane
            var emailContent = new EmailContent() { Title = "Zmiana hasła do rejestracji CKP", LinkDoZmiany = linkDOzmiany };


            logic.SendEmail(login, email, emailSettings, emailContent);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }



        public ActionResult DoLogin(string login, string password)
        {
            AppLogic logic = new AppLogic();
            if (!logic.LoginUser(login, password))
            {
                return Content("0");

            }
            return Content("1");
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {

            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            var idPrzedParsem = Request.QueryString["id"];

            var viewmodel = new ResetPasswordViewModel();
            Guid id;
            if (Guid.TryParse(idPrzedParsem, out id))
            {
                var dataFromDb = os.GetObjectByKey<EmailResetDataContainer>(id);
                if (dataFromDb != null)
                {
                    var isAuthorised = PasswordRetriveHelper.IsAuthorisedToChangePass(dataFromDb);
                    var czyJuzZmienilWczesniejHaslo = dataFromDb.IsUsed;
                    viewmodel.Login = dataFromDb.ClientLogin;
                    viewmodel.Email = dataFromDb.ClientEmail;
                    viewmodel.OidObiektuDanych = id.ToString();
                    viewmodel.CzyWyswietlic = isAuthorised && !czyJuzZmienilWczesniejHaslo;
                }
                else
                {
                    viewmodel.CzyWyswietlic = false;  
                }
            }
            else
            {
                viewmodel.CzyWyswietlic = false;
            }

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult ChangePassword(FormCollection collection)
        {
            var viewmodel = new ResetPasswordViewModel();
            var idPrzedParsem = collection["queryID"];
            var loginZFormy = collection["login"];
            var passwordZFormy = collection["password"];
            var passwordRepreatzFormy = collection["passwordRepeat"];

            Guid id;
            if (Guid.TryParse(idPrzedParsem, out id) && collection != null)
            {
                var os = Logic.Resolve<IDBProvider>().ObjectSpace;
                var dataFromDb = os.GetObjectByKey<EmailResetDataContainer>(id);

                if (dataFromDb != null)
                {
                    var kandydat = os.GetObjects<Kandydat>().Where(x => x.DaneOsobowe.Email == dataFromDb.ClientEmail && x.DaneOsobowe.PESEL == dataFromDb.ClientLogin).FirstOrDefault();

                    if (PasswordRetriveHelper.CheckIfCredentialAreOK(loginZFormy, passwordZFormy, passwordRepreatzFormy, dataFromDb, kandydat))
                    {

                        kandydat.DaneOsobowe.Salt = PasswordHelper.CreateSalt();
                        kandydat.DaneOsobowe.Haslo = PasswordHelper.Zakoduj(passwordZFormy, kandydat.DaneOsobowe.Salt);
                        viewmodel.CzyPotwierdzicZmianeHasla = true;
                        dataFromDb.IsUsed = true;
                        os.CommitChanges();
                    }
                    else
                    {
                        
                        viewmodel.CzyWyswietlic = false;
                        viewmodel.EwentualnyBlad = "Nastąpił błąd i hasło nie zostało zmienione. Proszę sprawdzić czy wpisano poprawny PESEL, hasła się zgadzją lub czy nie minęły 24 godziny od wysłania emaila z linkiem zmiany hasła.";
                    }

                }
                else
                {
                    viewmodel.CzyWyswietlic = false;
                }

            }

            return View(viewmodel);
        }

        public ActionResult LogOut()
        {

            FormsAuthentication.SignOut();
            Session.Abandon();

            return View("~/Views/Home/Index.cshtml");
        }



        
    }
}