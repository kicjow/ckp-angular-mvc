﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CKP.FirstBlood.MVC.Controllers
{
    public abstract class SuperController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            var x = filterContext.Exception;
            base.OnException(filterContext);
        }
    }
}