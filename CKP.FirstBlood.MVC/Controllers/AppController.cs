﻿using CKP.FirstBlood.Module.BusinessObjects;
using CKP.FirstBlood.Module.BusinessObjects.EmailProcessor;
using CKP.FirstBlood.Module.Common;
using CKP.FirstBlood.MVC.Models;
using CKP.FirstBlood.MVC.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CKP.FirstBlood.MVC.Controllers
{
    public class AppController : SuperController
    {
        // GET: App
        public ActionResult Index()
        {
            if (!Logic.Request.HasUser)
            {
                return RedirectToAction("Index", "Home");
            }

            //TODO USUNAC TEST
            //var emailsender = new EmailProcessor();
            //emailsender.ProcessEmail(new EmailSetting(), new EmailContent());

            ViewBag.Kursy = Logic.Resolve<IDBProvider>().ObjectSpace.GetObjects<Kurs>().ToList();
            ViewBag.Aplikacje = Logic.Request.User.Aplikacje.ToList();
            ViewBag.Kandydat = Logic.Request.User;

            return View();
        }

        [HttpPost, MyAuthorize]
        public ActionResult Specjalizacje()
        {
            Dictionary<string, string> result = new Dictionary<string, string>() { { "", "Wybierz" } };
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            os.GetObjects<Specjalizacja>().OrderBy(x => x.RodzajSpecjalonosci.Kod).ToList().ForEach(x => result.Add(/*string.Format("\"{0}\"", x.Kod)*/ x.Kod, string.Format("{0} - {1}", x.Nazwa, x.RodzajSpecjalonosci.Nazwa)));

            return Json(result);
        }

        [HttpPost, MyAuthorize]
        public ActionResult RodzajSpecjalnosci()
        {
            Dictionary<string, string> result = new Dictionary<string, string>() { { "", "Wybierz" } };

            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            os.GetObjects<RodzajSpecjalnosci>().OrderBy(x => x.Kod).ToList().ForEach(x => result.Add(x.Kod, x.Nazwa));

            return Json(result);
        }

        [HttpPost, MyAuthorize]
        public ActionResult TypyKursow()
        {
            Dictionary<string, string> result = new Dictionary<string, string>() { { "", "Wybierz" } };

            Enum.GetValues(typeof(TypyKursów)).Cast<TypyKursów>().ToList().ForEach(x => result.Add(((int)x).ToString(), x.ToString()));

            return Json(result);
        }

        [HttpPost, MyAuthorize]
        public ActionResult DaneOsobowe()
        {
            DaneOsoboweModel model = new DaneOsoboweModel();
            var osoba = Logic.Request.User;
            model.DataRozpoczecia = osoba.DaneOsobowe.RozpSpecjalizacji.HasValue ? osoba.DaneOsobowe.RozpSpecjalizacji.Value.ToString("dd-MM-yyyy") : "";
            model.DataZakonczenia = osoba.DaneOsobowe.PlanownyTerminZakSpecjaliz.HasValue ? osoba.DaneOsobowe.PlanownyTerminZakSpecjaliz.Value.ToString("dd-MM-yyyy") : "";
            model.Email = osoba.DaneOsobowe.Email ?? "";
            model.Imie = osoba.DaneOsobowe.Imię ?? "";
            model.Imie2 = osoba.DaneOsobowe.Imię2 ?? "";
            model.KodPocztowy = osoba.DaneOsobowe.KodPocztowy ?? "";
            model.Miejscowosc = osoba.DaneOsobowe.Miejscowość ?? "";
            model.Nazwisko = osoba.DaneOsobowe.Nazwisko ?? "";
            model.NumerPrawa = osoba.DaneOsobowe.NumerPrawaWykonywaniaZawodu ?? "";
            model.Poczta = osoba.DaneOsobowe.Poczta ?? "";
            model.Specjalizacja = osoba.DaneOsobowe.Specjalizacja != null ? osoba.DaneOsobowe.Specjalizacja.Kod : "";
            model.Obywatelstwo = osoba.DaneOsobowe.Obywatelstwo == null ? "" : osoba.DaneOsobowe.Obywatelstwo.Nazwa ?? "";
            model.Telefon = osoba.DaneOsobowe.Telefon ?? "";
            model.Ulica = osoba.DaneOsobowe.Ulica ?? "";
            model.Faktury = new Dictionary<string, FakturaModel>(osoba.DaneDoFaktury.Count + 1)
            {
                { "", new FakturaModel() 
                    {
                        Id = "",
                        Nazwa = "Wybierz",
                        Kod = "",
                        Miejscowosc = "",
                        Nip = "",
                        Ulica = "",
                    }
                }
            };
            foreach (KandydatDaneDoFaktury f in osoba.DaneDoFaktury)
            {
                model.Faktury.Add('f' + f.Oid.ToString(), new FakturaModel()
                {
                    Id = 'f' + f.Oid.ToString(),
                    Nazwa = f.Nazwa ?? "",
                    Kod = f.KodPocztowy ?? "",
                    Nip = f.NIP ?? "",
                    Ulica = f.Ulica ?? "",
                    Miejscowosc = f.Miejscowość ?? ""
                });
            }

            return Json(model);
        }

        [HttpPost, MyAuthorize]
        public ActionResult Kursy()
        {
            var kursy = Logic.Resolve<IDBProvider>().ObjectSpace.GetObjects<Kurs>().ToList();

            Dictionary<string, object> model = new Dictionary<string, object>(kursy.Count);
            foreach (var kurs in kursy)
            {

                var typkursu = ((int)(kurs.TypKursu)).ToString();//.GetDescription();
                model.Add(kurs.Oid.ToString(), new
                {
                    Specjalizacja = kurs.Specjalizacja == null ? "" : kurs.Specjalizacja.Kod,
                    RodzajSpecjalnosci = kurs.Specjalizacja.RodzajSpecjalonosci.Kod,
                    Nazwa = kurs.Nazwa.ToLower(),
                    Typ = typkursu
                });
            }
            return Json(model);
        }

        [HttpPost, MyAuthorize]
        public ActionResult Aplikacje()
        {
            List<AplikacjaModel> aplikacje = new List<AplikacjaModel>();
            foreach (var bApp in Logic.Request.User.Aplikacje)
            {
                DaneOsoboweModel daneOsoboweModel = new DaneOsoboweModel();
                daneOsoboweModel.DataRozpoczecia = bApp.DaneOsobowe.RozpSpecjalizacji.HasValue ? bApp.DaneOsobowe.RozpSpecjalizacji.Value.ToString("dd-MM-yyyy") : "";
                daneOsoboweModel.DataZakonczenia = bApp.DaneOsobowe.PlanownyTerminZakSpecjaliz.HasValue ? bApp.DaneOsobowe.PlanownyTerminZakSpecjaliz.Value.ToString("dd-MM-yyyy") : "";
                daneOsoboweModel.Email = bApp.DaneOsobowe.Email ?? "";
                daneOsoboweModel.Imie = bApp.DaneOsobowe.Imię ?? "";
                daneOsoboweModel.Imie2 = bApp.DaneOsobowe.Imię2 ?? "";
                daneOsoboweModel.KodPocztowy = bApp.DaneOsobowe.KodPocztowy ?? "";
                daneOsoboweModel.Miejscowosc = bApp.DaneOsobowe.Miejscowość ?? "";
                daneOsoboweModel.Nazwisko = bApp.DaneOsobowe.Nazwisko ?? "";
                daneOsoboweModel.NumerPrawa = bApp.DaneOsobowe.NumerPrawaWykonywaniaZawodu ?? "";
                daneOsoboweModel.Poczta = bApp.DaneOsobowe.Poczta ?? "";
                daneOsoboweModel.Obywatelstwo = bApp.DaneOsobowe.Obywatelstwo == null ? "" : bApp.DaneOsobowe.Obywatelstwo.Nazwa ?? "";
                daneOsoboweModel.Specjalizacja = bApp.DaneOsobowe.Specjalizacja != null ? bApp.DaneOsobowe.Specjalizacja.Kod : "";
                daneOsoboweModel.Telefon = bApp.DaneOsobowe.Telefon ?? "";
                daneOsoboweModel.Ulica = bApp.DaneOsobowe.Ulica ?? "";

                FakturaModel fakturaModel = new FakturaModel()
                {
                    Id = string.Empty,
                    Kod = bApp.DaneDoFaktury.KodPocztowy,
                    Miejscowosc = bApp.DaneDoFaktury.Miejscowość,
                    Nazwa = string.Empty,
                    Nip = bApp.DaneDoFaktury.NIP,
                    Ulica = bApp.DaneDoFaktury.Ulica,
                };


                var app = new AplikacjaModel()
                {
                    DaneOsobowe = daneOsoboweModel,
                    DataRozpoczecia = bApp.Termin.Początek.HasValue ? bApp.Termin.Początek.Value.ToShortDateString() : "",
                    DataZakonczenia = bApp.Termin.Koniec.HasValue ? bApp.Termin.Koniec.Value.ToShortDateString() : "",
                    KursNazwa = bApp.Termin.Kurs.Nazwa,
                    RodzajAplikacjiKod = ((int)bApp.Termin.Kurs.TypKursu).ToString(),
                    RodzajAplikacjiNazwa = bApp.Termin.Kurs.TypKursu.ToString(),
                    CzyRodzajKursuDoFaktury = KursHelper.SprawdzCzyMaBycWidocznaFaktura(bApp),
                    IDKursu = bApp.Termin.Oid.ToString(),
                    Status = bApp.Wypelniona ? 1 : 0,
                    Faktura = fakturaModel,
                    OtwartaEdycja = bApp.Termin.RejestracjaOtwarta,
                };
                aplikacje.Add(app);
            }

            return Json(new { Items = aplikacje });
        }

       

        [HttpPost, MyAuthorize]
        public ActionResult UsunAplikacje(AplikacjaModel model)
        {
            var user = Logic.Request.User;
            var aplikacja = user.Aplikacje.FirstOrDefault(x => x.Termin.Oid.ToString() == model.IDKursu);
            if (aplikacja != null)
            {
                user.Aplikacje.Remove(aplikacja);
            }
            Logic.Resolve<IDBProvider>().ObjectSpace.CommitChanges();

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
        }

        [HttpPost, MyAuthorize]
        public ActionResult DodajAplikacje(AplikacjaModel model)
        {
            var user = Logic.Request.User;
            Guid guid;
            Aplikacja result = null;
            if (!Guid.TryParse(model.IDKursu, out guid))
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, "KursID Invalid syntax");
            }
            if ((result = user.Aplikacje.FirstOrDefault(x => x.Termin.Oid == guid)) != null)
            {
                return Content(result.Oid.ToString());
            }
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            var termin = os.GetObjectByKey<TerminKursu>(guid);
            if (termin == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest, "KursID not found");
            }

            result = os.CreateObject<Aplikacja>();
            result.Termin = termin;
            result.DaneDoFaktury = os.CreateObject<DaneDoFaktury>();
            result.DataRozpoczeciaSpecjalizacji = termin.Początek;
            result.PlanowanyTerminZakonczeniaSpecjalizacji = termin.Koniec;
            result.Kandydat = user;
            result.PotwierdzenieOswiadczenia = false;
            result.DaneOsobowe = os.CreateObject<DaneOsobowe>();
            result.DaneOsobowe.Imię = model.DaneOsobowe.Imie;
            result.DaneOsobowe.Nazwisko = model.DaneOsobowe.Nazwisko;
            result.DaneOsobowe.DataUrodzenia = user.DaneOsobowe.DataUrodzenia;
            result.DaneOsobowe.DokumentNumer = user.DaneOsobowe.DokumentNumer;
            result.DaneOsobowe.Email = model.DaneOsobowe.Email;
            result.DaneOsobowe.Imię2 = model.DaneOsobowe.Imie2;
            result.DaneOsobowe.KodPocztowy = model.DaneOsobowe.KodPocztowy;
            result.DaneOsobowe.Miejscowość = model.DaneOsobowe.Miejscowosc;
            result.DaneOsobowe.Newsletter = true;
            result.DaneOsobowe.NumerPrawaWykonywaniaZawodu = model.DaneOsobowe.NumerPrawa;
            result.DaneOsobowe.PESEL = user.DaneOsobowe.PESEL;
            result.DaneOsobowe.NieposiadaPESEL = user.DaneOsobowe.NieposiadaPESEL;
            result.DaneOsobowe.Płeć = user.DaneOsobowe.Płeć;
            result.DaneOsobowe.Poczta = user.DaneOsobowe.Poczta;
            result.DaneOsobowe.RodzajDokumentu = user.DaneOsobowe.RodzajDokumentu;
            result.DaneOsobowe.Telefon = model.DaneOsobowe.Telefon;
            result.DaneOsobowe.Ulica = model.DaneOsobowe.Ulica;
            DateTime dataRozpoczecia, dataZakonczenia;
            bool rozpParsed = DateTime.TryParseExact(model.DaneOsobowe.DataRozpoczecia, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataRozpoczecia);
            bool zakParsed = DateTime.TryParseExact(model.DaneOsobowe.DataZakonczenia, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataZakonczenia);
            result.DaneOsobowe.RozpSpecjalizacji = rozpParsed ? (DateTime?)dataRozpoczecia : null;
            result.DaneOsobowe.PlanownyTerminZakSpecjaliz = zakParsed ? (DateTime?)dataZakonczenia : null;
            result.DaneOsobowe.Specjalizacja = DictBase.FromCode<Specjalizacja>(model.DaneOsobowe.Specjalizacja, os);

            os.CommitChanges();
            return Content(result.Oid.ToString());
        }

        [HttpPost, MyAuthorize]
        public ActionResult SaveDaneOsobowe(DaneOsoboweModel model)
        {
            try
            {
                var os = Logic.Resolve<IDBProvider>().ObjectSpace;

                var osoba = Logic.Request.User;
                osoba.DaneOsobowe.Imię = model.Imie;
                osoba.DaneOsobowe.Imię2 = model.Imie2;
                osoba.DaneOsobowe.Nazwisko = model.Nazwisko;
                osoba.DaneOsobowe.Miejscowość = model.Miejscowosc;
                osoba.DaneOsobowe.Ulica = model.Ulica;
                osoba.DaneOsobowe.KodPocztowy = model.KodPocztowy;
                osoba.DaneOsobowe.Poczta = model.Poczta;
                osoba.DaneOsobowe.Telefon = model.Telefon;
                osoba.DaneOsobowe.Email = model.Email;
                osoba.DaneOsobowe.Specjalizacja = DictBase.FromCode<Specjalizacja>(model.Specjalizacja, os);
                osoba.DaneOsobowe.RozpSpecjalizacji = string.IsNullOrEmpty(model.DataRozpoczecia) ? (DateTime?)null :
                    DateTime.ParseExact(model.DataRozpoczecia, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                osoba.DaneOsobowe.PlanownyTerminZakSpecjaliz = string.IsNullOrEmpty(model.DataZakonczenia) ? (DateTime?)null :
                    DateTime.ParseExact(model.DataZakonczenia, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                osoba.DaneOsobowe.NumerPrawaWykonywaniaZawodu = model.NumerPrawa;

                List<Guid> oids = new List<Guid>();
                foreach (var f in model.Faktury)
                {
                    if (f.Key == string.Empty) continue;
                    if (f.Key.StartsWith("n"))
                    {
                        var fNowa = os.CreateObject<KandydatDaneDoFaktury>();
                        fNowa.KodPocztowy = f.Value.Kod;
                        fNowa.Miejscowość = f.Value.Miejscowosc;
                        fNowa.Nazwa = f.Value.Nazwa;
                        fNowa.NIP = f.Value.Nip;
                        fNowa.Ulica = f.Value.Ulica;
                        osoba.DaneDoFaktury.Add(fNowa);
                    }
                    else if (f.Key.StartsWith("f"))
                    {
                        var oid = Guid.Parse(f.Key.Substring(1));
                        oids.Add(oid);
                        var fEx = osoba.DaneDoFaktury.FirstOrDefault(x => x.Oid == oid);
                        fEx.KodPocztowy = f.Value.Kod;
                        fEx.Miejscowość = f.Value.Miejscowosc;
                        fEx.Nazwa = f.Value.Nazwa;
                        fEx.NIP = f.Value.Nip;
                        fEx.Ulica = f.Value.Ulica;
                    }
                }
                for (int i = osoba.DaneDoFaktury.Count - 1; i >= 0; i--)
                {
                    if (osoba.DaneDoFaktury[i].Oid != Guid.Empty && !oids.Contains(osoba.DaneDoFaktury[i].Oid))
                    {
                        osoba.DaneDoFaktury.Remove(osoba.DaneDoFaktury[i]);
                    }
                }
                os.CommitChanges();
            }
            catch
            {
                return Content("0");
            }
            return Content("1");
        }
        public ActionResult ZapiszAplikacje(AplikacjaModel model)
        {
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            var user = Logic.Request.User;
            Guid terimOid;
            if (!Guid.TryParse(model.IDKursu, out terimOid))
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            var termin = os.GetObjectByKey<TerminKursu>(terimOid);
            if (termin == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            if (!termin.RejestracjaOtwarta)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.NotModified, "Edycja zamknięta");
            }

            Aplikacja result = user.Aplikacje.FirstOrDefault(x => x.Termin == termin);
            if (result == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            result.Termin = termin;
            result.DaneDoFaktury = os.CreateObject<DaneDoFaktury>();
            result.DataRozpoczeciaSpecjalizacji = termin.Początek;
            result.PlanowanyTerminZakonczeniaSpecjalizacji = termin.Koniec;
            result.PotwierdzenieOswiadczenia = false;
            
            result.DaneOsobowe = os.CreateObject<DaneOsobowe>();
            result.DaneOsobowe.Imię = model.DaneOsobowe.Imie;
            result.DaneOsobowe.Nazwisko = model.DaneOsobowe.Nazwisko;
            result.DaneOsobowe.Email = model.DaneOsobowe.Email;
            result.DaneOsobowe.Imię2 = model.DaneOsobowe.Imie2;
            result.DaneOsobowe.KodPocztowy = model.DaneOsobowe.KodPocztowy;
            result.DaneOsobowe.Miejscowość = model.DaneOsobowe.Miejscowosc;
            result.DaneOsobowe.Newsletter = true;
            result.DaneOsobowe.NumerPrawaWykonywaniaZawodu = model.DaneOsobowe.NumerPrawa;
            result.DaneOsobowe.Poczta = model.DaneOsobowe.Poczta;
            result.DaneOsobowe.Telefon = model.DaneOsobowe.Telefon;
            result.DaneOsobowe.Ulica = model.DaneOsobowe.Ulica;
            DateTime dataRozpoczecia, dataZakonczenia;
            bool rozpParsed = DateTime.TryParseExact(model.DaneOsobowe.DataRozpoczecia, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataRozpoczecia);
            bool zakParsed = DateTime.TryParseExact(model.DaneOsobowe.DataZakonczenia, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out dataZakonczenia);
            result.DaneOsobowe.RozpSpecjalizacji = rozpParsed ? (DateTime?)dataRozpoczecia : null;
            result.DaneOsobowe.PlanownyTerminZakSpecjaliz = zakParsed ? (DateTime?)dataZakonczenia : null;
            result.DaneOsobowe.Specjalizacja = DictBase.FromCode<Specjalizacja>(model.DaneOsobowe.Specjalizacja, os);
            result.DaneDoFaktury.KodPocztowy = model.Faktura.Kod;
            result.DaneDoFaktury.Miejscowość = model.Faktura.Miejscowosc;
            result.DaneDoFaktury.NIP = model.Faktura.Nip;
            result.DaneDoFaktury.Ulica = model.Faktura.Ulica;

            os.CommitChanges();

            return new HttpStatusCodeResult(System.Net.HttpStatusCode.OK);
        }
    }
}

