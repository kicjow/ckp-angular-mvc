﻿using CKP.FirstBlood.Module.BusinessObjects;
using CKP.FirstBlood.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CKP.FirstBlood.MVC.Controllers
{
    public class HomeController : SuperController
    {
        public ActionResult Index()
        {
            if (Logic.Request.HasUser)
            {
                return RedirectToAction("Index", "App");
            }
            return View();
        }

        [HttpPost]
        public ActionResult Obywatelstwa()
        {
            Dictionary<string, string> result = new Dictionary<string, string>() { { "", "Wybierz" }, { "PL", "polskie" } };
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            os.GetObjects<Obywatelstwo>().OrderBy(x => x.Kod).ToList().ForEach(x => result.Add(/*string.Format("\"{0}\"", x.Kod)*/ x.Kod, x.Nazwa));

            return Json(result);
        }
    }
}