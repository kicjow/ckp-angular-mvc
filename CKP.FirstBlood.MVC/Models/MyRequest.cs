﻿using CKP.FirstBlood.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace CKP.FirstBlood.MVC.Models
{
    public interface IMyRequest
    {
        bool HasUser { get; }
        Kandydat User { get; }
        string UserName { get; }
    }

    public class MyRequest : IMyRequest
    {
        Kandydat _kandydat;

        public MyRequest()
        {
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            var userName = HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrEmpty(userName))
            {
                _kandydat = os.GetObjectByKey<Kandydat>(Guid.Parse(userName));
            }
        }

        public bool HasUser
        {
            get { return _kandydat != null; }
        }

        public Kandydat User
        {
            get { return _kandydat; }
        }

        public string UserName
        {
            get { return _kandydat == null ? "Gość" : (string.Format("{0} {1} {2}", _kandydat.DaneOsobowe.Imię, _kandydat.DaneOsobowe.Imię2, _kandydat.DaneOsobowe.Nazwisko)); }
        }
    }
}