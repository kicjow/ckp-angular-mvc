﻿using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models
{
    public static class Logic
    {
        static WindsorContainer _container;

        static Logic()
        {
            _container = new WindsorContainer();
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public static IMyRequest Request
        {
            get
            {
                return Resolve<IMyRequest>();
            }
        }

        public static IWindsorContainer Container
        {
            get
            {
                return _container;
            }
        }
    }
}