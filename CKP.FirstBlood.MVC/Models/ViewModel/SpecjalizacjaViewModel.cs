﻿using CKP.FirstBlood.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models.ViewModel
{
    public class SpecjalizacjaViewModel
    {
        public string Nazwa;
        public string RodzajSpecjalnosciNazwa;
        public string Kod;
        public string RodzajSpecjalnoscKod;
    }
}