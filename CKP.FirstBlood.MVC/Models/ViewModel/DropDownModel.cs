﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models.ViewModel
{
    public class DropDownModel
    {
        public string Caption { get; set; }
        public string AngularModelName { get; set; }
        public string DataSourceName { get; set; }
        public bool Required { get; set; }
        public string InitailValue { get; set; }
        public string DisableQuerry { get; set; }
        public string OnChangeValue { get; set; }
    }
}