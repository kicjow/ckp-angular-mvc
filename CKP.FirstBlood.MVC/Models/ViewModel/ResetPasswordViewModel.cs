﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models.ViewModel
{
    public class ResetPasswordViewModel
    {

        public ResetPasswordViewModel()
        {
            CzyWyswietlic = false;
            CzyPotwierdzicZmianeHasla = false;
        }
        public string Login { get; set; }
        public string Email { get; set; }
        public string OidObiektuDanych { get; set; }
        public bool CzyWyswietlic { get; set; }
        public bool CzyPotwierdzicZmianeHasla { get; set; }
        public string EwentualnyBlad { get; set; }
    }
}