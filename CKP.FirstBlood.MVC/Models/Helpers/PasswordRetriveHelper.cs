﻿using CKP.FirstBlood.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models.Helpers
{
    public static class PasswordRetriveHelper
    {

        public static bool IsAuthorisedToChangePass(EmailResetDataContainer dataFromDb)
        {
            var isused = dataFromDb.IsUsed;
            var isnotnull = dataFromDb.RequestCreationDateTime.HasValue;
            var isTimeStampValid = (DateTime.Now - dataFromDb.RequestCreationDateTime.Value).TotalHours <= 24;
            var doescandidateexists = CheckIfCandidateExist(dataFromDb.ClientEmail, dataFromDb.ClientLogin);

            return (!isused && isnotnull && isTimeStampValid && doescandidateexists);

        }

        private static bool CheckIfCandidateExist(string email, string login)
        {
            var os = Logic.Resolve<IDBProvider>().ObjectSpace;
            var kandydat = os.GetObjects<Kandydat>().Where(x => x.DaneOsobowe.PESEL == login && x.DaneOsobowe.Email == email);

            return kandydat != null;
        }

        public static bool CheckIfCredentialAreOK(string loginZFormy, string passwordZFormy, string passwordRepreatzFormy, EmailResetDataContainer dataFromDb, Kandydat kandydat)
        {
            return kandydat != null && loginZFormy == dataFromDb.ClientLogin && passwordRepreatzFormy == passwordZFormy;
        }
    }
}