﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Helpers;

namespace CKP.FirstBlood.MVC.Models
{
    public static class PasswordHelper
    {
        public static string Zakoduj(string password, string salt)
        {
            var passwordToHash = password + salt;
            var hashedPassword = Crypto.SHA256(passwordToHash);
            return hashedPassword;
        }

        internal static string CreateSalt()
        {
            var salt = Crypto.GenerateSalt();
            return salt;
        }
    }
}