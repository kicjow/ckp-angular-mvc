﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CKP.FirstBlood.MVC.Models
{
    public class DaneOsoboweModel : IJsonModel
    {
        public string Imie { get; set; }
        public string Imie2 { get; set; }
        public string Nazwisko { get; set; }
        public string Miejscowosc { get; set; }
        public string Ulica { get; set; }
        public string KodPocztowy { get; set; }
        public string Poczta { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string Specjalizacja { get; set; }
        public string DataRozpoczecia { get; set; }
        public string DataZakonczenia { get; set; }
        public string NumerPrawa { get; set; }
        public string Obywatelstwo { get; set; }
        public Dictionary<string, FakturaModel> Faktury { get; set; }
    }

    public class FakturaModel
    {
        public string Kod { get; set; }
        public string Id { get; set; }
        public string Miejscowosc { get; set; }
        public string Ulica { get; set; }
        public string Nip { get; set; }
        public string Nazwa { get; set; }
    }
}