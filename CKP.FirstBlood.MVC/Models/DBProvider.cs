﻿using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models
{
    public interface IDBProvider
    {
        IObjectSpace ObjectSpace { get; }
    }

    internal class DBProviderImpl : IDBProvider
    {
        IObjectSpace _os;

        public DBProviderImpl()
        {
             _os = Logic.Resolve<IObjectSpaceProvider>().CreateObjectSpace();
        }

        public IObjectSpace ObjectSpace
        {
            get { return _os; }
        }
    }
}