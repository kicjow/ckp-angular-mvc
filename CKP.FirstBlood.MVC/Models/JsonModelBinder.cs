﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CKP.FirstBlood.MVC.Models
{
    public class JsonModelBinder : IModelBinderProvider, IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {

            var attempetdValue = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue;
            var modelType = bindingContext.ModelType;

            var result = new JavaScriptSerializer().Deserialize(attempetdValue, modelType);
            return result;
        }

        public IModelBinder GetBinder(Type modelType)
        {
            if (modelType.GetInterface(typeof(IJsonModel).FullName) != null)
            {
                return this;
            }
            return null;
        }
    }
}