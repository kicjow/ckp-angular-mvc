﻿using CKP.FirstBlood.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using CKP.FirstBlood.Module;
using System.Security.Cryptography;
using CKP.FirstBlood.Module.BusinessObjects.EmailProcessor;

namespace CKP.FirstBlood.MVC.Models
{
    public class AppLogic
    {
        public IObjectSpace ObjectSpace
        {
            get
            {
                return Logic.Resolve<IDBProvider>().ObjectSpace;
            }
        }

        public void RegisterNewAccount(NameValueCollection formData)
        {
            var os = ObjectSpace;
            var salt = PasswordHelper.CreateSalt();
            Kandydat kandydat = os.CreateObject<Kandydat>();
            kandydat.DaneOsobowe = os.CreateObject<DaneOsobowe>();
            kandydat.DaneOsobowe.Imię = formData["imie"].Trim();
            kandydat.DaneOsobowe.Nazwisko = formData["nazwisko"].Trim();
            kandydat.DaneOsobowe.Płeć = DictBase.FromCode<Płeć>(formData["plec"], os);
            kandydat.DaneOsobowe.DataUrodzenia = DateTime.ParseExact(formData["dataUrodzenia"], "dd-MM-yyyy", CultureInfo.InvariantCulture);
            kandydat.DaneOsobowe.NieposiadaPESEL = formData["brakPesel"] == null ? false : true;
            kandydat.DaneOsobowe.Salt = salt;
            kandydat.DaneOsobowe.Obywatelstwo = DictBase.FromCode<Obywatelstwo>("dObyw_" + formData["obywatelstwo"], os);

            if (!kandydat.DaneOsobowe.NieposiadaPESEL)
            {
                kandydat.DaneOsobowe.PESEL = formData["pesel"].Trim();
            }
            //  kandydat.DaneOsobowe.DokumentNumer = formData["dokumentNumer"].Trim();
            kandydat.DaneOsobowe.RodzajDokumentu = DictBase.FromCode<RodzajDokumentu>(formData["dokumentRodzaj"], os);
            kandydat.DaneOsobowe.Email = formData["email"].Trim();
            kandydat.DaneOsobowe.Haslo = PasswordHelper.Zakoduj(formData["password"], kandydat.DaneOsobowe.Salt);
        }

        public bool LoginUser(string login, string password)
        {
            var os = ObjectSpace;
            var kandydaci = os.GetObjects<Kandydat>();
            Kandydat kandydat = os.FindObject<Kandydat>(x => x.DaneOsobowe.PESEL == login);

            if (kandydat == null)
            {
                return false;
            }
            else if (kandydat.DaneOsobowe != null)
            {
                var ePassword = PasswordHelper.Zakoduj(password, kandydat.DaneOsobowe.Salt);
                if (kandydat.DaneOsobowe.Haslo != ePassword)
                {
                    return false;
                }
            }
            else if (kandydat.DaneOsobowe == null)
            {
                return false;
            }

            FormsAuthentication.SetAuthCookie(kandydat.Oid.ToString(), true);
            return true;
        }

        public void ValidateRegisterForm(NameValueCollection formData, IList<string> errors)
        {
            if (string.IsNullOrWhiteSpace(formData["imie"]))
            {
                errors.Add("Imię nie może być puste");
            }
            if (string.IsNullOrWhiteSpace(formData["nazwisko"]))
            {
                errors.Add("Nazwisko nie może być puste");
            }
            if (string.IsNullOrWhiteSpace(formData["plec"]))
            {
                errors.Add("Płeć wybór jest obowiązkowy");
            }
            if (string.IsNullOrWhiteSpace(formData["dataUrodzenia"]))
            {
                errors.Add("Data urodzenia jest wymagana");
            }
            if (string.IsNullOrWhiteSpace(formData["brakPesel"]))
            {
                if (string.IsNullOrWhiteSpace(formData["pesel"]))
                {
                    errors.Add("PESEL nie może być pusty");
                }

                var pesel = formData["pesel"];// ?? "";

                //var os = ObjectSpace;
                //var daneOsobowe = os.GetObjects<DaneOsobowe>();

                //if (daneOsobowe.Any(x=> x.PESEL == pesel))
                //{
                //    errors.Add("Już istnieje konto o takim numerze PESEL");
                //}

                var dataUrodzenia = formData["dataUrodzenia"] ?? "";
                var plec = formData["plec"] ?? "";

                if (pesel.Length != 11)
                {
                    errors.Add("Zła długość PESEL");
                }
                try
                {
                    if (dataUrodzenia.Substring(8, 2) + dataUrodzenia.Substring(3, 2) + dataUrodzenia.Substring(0, 2) != pesel.Substring(0, 6))
                    {
                        errors.Add("Data urodzenia i PESEL nie są zgodne");
                    }
                }
                catch (Exception)
                {
                    errors.Add("Data urodzenia i PESEL nie są zgodne");
                }
                try
                {
                    if ((plec == "plec_k" && pesel[9] % 2 != 0) || (plec == "plec_m" && pesel[9] % 2 != 1))
                    {
                        errors.Add("Płeć i PESEL nie są zgodne");
                    }
                }
                catch (Exception)
                {
                    errors.Add("Płeć i PESEL nie są zgodne");
                }
                try
                {
                    var kontrolna = Char.GetNumericValue(pesel[0]) + 3 * Char.GetNumericValue(pesel[1]) + 7 * Char.GetNumericValue(pesel[2]) + 9 * Char.GetNumericValue(pesel[3]) + Char.GetNumericValue(pesel[4]) + 3 * Char.GetNumericValue(pesel[5]) + 7 * Char.GetNumericValue(pesel[6])
                        + 9 * Char.GetNumericValue(pesel[7]) + Char.GetNumericValue(pesel[8]) + 3 * Char.GetNumericValue(pesel[9]) + Char.GetNumericValue(pesel[10]);
                    if (kontrolna % 10 != 0)
                    {
                        errors.Add("PESEL jest błędny");
                    }
                }
                catch
                {
                    errors.Add("PESEL jest błędny");
                }
                if (ObjectSpace.FindObject<Kandydat>(x => x.DaneOsobowe.PESEL == formData["pesel"]) != null)
                {
                    errors.Add("Konto o podanym numerze PESEL już istnieje");
                }
            }
            //if (string.IsNullOrWhiteSpace(formData["dokumentRodzaj"]))
            //{
            //    errors.Add("Rodzaj wybór jest obowiązkowy");
            //}
            //if (string.IsNullOrWhiteSpace(formData["dokumentNumer"]))
            //{
            //    errors.Add("Numer dokumentu jest wymagany");
            //}
            //if (ObjectSpace.FindObject<Kandydat>(x => x.DaneOsobowe.DokumentNumer == formData["dokumentNumer"]) != null)
            //{
            //    errors.Add("Konto o podanym numerze dokumentu już istnieje");
            //}
            if (string.IsNullOrWhiteSpace(formData["email"]))
            {
                errors.Add("Email jest wymagany");
            }
            if (!Regex.Match(formData["email"], @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {
                errors.Add("Email ma błędny format");
            }
            if (string.IsNullOrWhiteSpace(formData["password"]))
            {
                errors.Add("Hasło jest wymagane");
            }
            if (formData["password"] != formData["passwordRepeat"])
            {
                errors.Add("Hasła nie są takie same");
            }
        }

        public void Save()
        {
            ObjectSpace.CommitChanges();
        }



        internal void SendEmail(string login, string email, EmailSetting settings, EmailContent content)
        {
            var emailsender = new EmailProcessor();
            emailsender.ProcessEmail(settings, content);
        }
    }
}