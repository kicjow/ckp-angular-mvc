﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CKP.FirstBlood.MVC.Models
{
    public class AplikacjaModel : IJsonModel
    {
        public DaneOsoboweModel DaneOsobowe { get; set; }
        public int Status { get; set; }
        public string DataRozpoczecia { get; set; }
        public string DataZakonczenia { get; set; }
        public string KursNazwa { get; set; }
        public string RodzajAplikacjiNazwa { get; set; }
        public bool CzyRodzajKursuDoFaktury { get; set; }
        public string RodzajAplikacjiKod { get; set; }
        public string IDKursu { get; set; }
        public FakturaModel Faktura { get; set; }
        public bool OtwartaEdycja { get; set; }
    }
}