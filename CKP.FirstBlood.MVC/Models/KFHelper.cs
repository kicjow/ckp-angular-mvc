﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace CKP.FirstBlood.MVC.Models
{
    public class KFHelper
    {
        HtmlHelper _htmlHelper;
        public KFHelper(HtmlHelper htmlHelper)
        {
            _htmlHelper = htmlHelper;
        }

        public MvcHtmlString DropDown(string caption, string modelFieldName, string dataSourceName, bool required = true, string onChangeValue = "", string selectedValue = "", string disableQuerry = "")
        {
            return _htmlHelper.Partial("DropDown", new ViewModel.DropDownModel() { AngularModelName = modelFieldName, Caption = caption, DataSourceName = dataSourceName, InitailValue = selectedValue, Required = required, DisableQuerry = disableQuerry, OnChangeValue = onChangeValue });

            //StringBuilder builder = new StringBuilder("<div class=\"input-group\"><span class=\"input-group-addon\">");
            //builder.Append(caption);
            //builder.Append("</span><div class=\"input-group-btn input-group-fullSize\"><button type=\"button\" class=\"btn btn-default dropdown-toggle input-group-fullSize\" data-toggle=\"dropdown\" aria-expanded=\"false\">{{");
            //builder.Append(dataSourceName);
            //builder.Append("[");
            //builder.Append(modelFieldName);
            //builder.Append("]}} <span class='caret'></span></button><ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\"><li ng-repeat=\"(k, v) in ");
            //builder.Append(dataSourceName);
            //builder.Append("\"><a href=\"\" ng-click=\"$parent.");
            //builder.Append(modelFieldName);
            //builder.Append(" = k\">{{v}}</a></li></ul></div></div>");
            //return new MvcHtmlString(builder.ToString());


            //<div class="input-group">
            //    <span class="input-group-addon">Rodzaj</span>
            //    <div class="input-group-btn input-group-fullSize">
            //        <button type="button" class="btn btn-default dropdown-toggle input-group-fullSize" data-toggle="dropdown" aria-expanded="false">{{rodzajDokumentuArray[dokumentRodzaj]}} <span class='caret'></span></button>
            //        <ul class="dropdown-menu dropdown-menu-right" role="menu">
            //            <li ng-repeat="(k, v) in rodzajDokumentuArray">
            //                <a href="" ng-click="$parent.dokumentRodzaj = k">{{v}}</a>
            //            </li>
            //        </ul>
            //    </div>
            //</div>
        }
    }

    public static class KFHelperExtension
    {
        public static KFHelper KF(this HtmlHelper htmlHelper)
        {
            return new KFHelper(htmlHelper);
        }
    }
}