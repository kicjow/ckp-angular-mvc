﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CKP.FirstBlood.MVC.Models
{
    public class DateTimeModelBinder : IModelBinder, IModelBinderProvider
    {
        public IModelBinder GetBinder(Type modelType)
        {
            if (modelType == typeof(DateTime))
                return this;
            return null;
        }

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valProvider = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valProvider == null || string.IsNullOrEmpty(valProvider.AttemptedValue))
            {
                bindingContext.ModelState.AddModelError(bindingContext.ModelName, "Wartość nie może być pusta");
                return default(DateTime);
            }

            DateTime result;
            if (DateTime.TryParseExact(valProvider.AttemptedValue, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out result))
            {
                return result;
            }

            bindingContext.ModelState.AddModelError(bindingContext.ModelName, "Nieprawidłowy format daty");
            return default(DateTime);
        }
    }
}