﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace CKP.FirstBlood.Module
{
    public static class ObjectSpaceExtensions
    {/*
        public static T FindObject<T, TProperty>(this IObjectSpace objectSpace, TProperty value, Expression<Func<T, TProperty>> propertyLambda) where T : XPObject
        {
            PropertyInfo propInfo = ForReflectionHelper.GetPropertyInfo<T, TProperty>(default(T), propertyLambda);
            BinaryOperator oper = new BinaryOperator(propInfo.Name, value);
            return objectSpace.FindObject<T>(oper);
        }

        
        public static T FindObject<T>(this IObjectSpace objectSpace, object value, Expression<Func<T, object>> propertyLambda) where T : XPObject
        {
            return FindObject<T, object>(objectSpace, value, propertyLambda);
        }
        */

        public static Session GetSession(this IObjectSpace objectSpace)
        {
            return ((XPObjectSpace)objectSpace).Session;
        }

        public static UnitOfWork GetUnitOfWork(this IObjectSpace objectSpace)
        {
            return ((XPObjectSpace)objectSpace).Session as UnitOfWork;
        }

        public static XPQuery<T> Query<T>(this IObjectSpace objectSpace)
        {
            return objectSpace.GetSession().Query<T>();
        }


        /// <summary>
        /// Od razu ładuje do pamięci wszystkie obiekty. Używa Query więc może nie działać dla sytuacji, których nie da się zrobić na bazie
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="objectSpace"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static IList<TSource> GetObjects<TSource>(this IObjectSpace objectSpace, Expression<Func<TSource, bool>> predicate)
        {
            var queryable = objectSpace.Query<TSource>();
            var result = queryable.Where(predicate).ToList();
            return result;
        }





        /*
        /// <summary>
        /// To działa raczej na bazie
        /// </summary>
        /// <param name="objectSpace"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static TSource FindObject<TSource>(this IObjectSpace objectSpace, Expression<Func<TSource, bool>> predicate)
        {
            Stopwatch watch = new Stopwatch();

            var queryable = objectSpace.Query<TSource>();
            var result = queryable.FirstOrDefault(predicate);
            return result;
        }
*/

        public static ClassType FindObject<ClassType>(this IObjectSpace objectSpace, Expression<Func<ClassType, bool>> expression)
        {
            Session session = objectSpace.GetSession();
            return (ClassType)session.FindObject(typeof(ClassType), new XPQuery<ClassType>(session).TransformExpression(expression), false);
        }


        public static CriteriaOperator ToCriteria<ClassType>(this IObjectSpace objectSpace, Expression<Func<ClassType, bool>> expression)
        {
            Session session = objectSpace.GetSession();
            return new XPQuery<ClassType>(session).TransformExpression(expression);
        }

        public static CriteriaOperator ToCriteria<ClassType>(this Session session, Expression<Func<ClassType, bool>> expression)
        {
            return new XPQuery<ClassType>(session).TransformExpression(expression);
        }

        public static XPCollection<ClassType> GetXPCollection<ClassType>(this Session session, Expression<Func<ClassType, bool>> expression)
        {
            return new XPCollection<ClassType>(session, session.ToCriteria<ClassType>(expression));
        }


        public static ClassType FindObject<ClassType>(this Session session, Expression<Func<ClassType, bool>> expression)
        {
            return (ClassType)session.FindObject(typeof(ClassType), new XPQuery<ClassType>(session).TransformExpression(expression), false);
        }


        /*
        public static void EnableNotRefresing(this IObjectSpace objectSpace)
        {
            CoreService.NotRefreshingSession.AddObjectSpace(objectSpace);
            objectSpace.Disposed += delegate(object sender, EventArgs e) { CoreService.NotRefreshingSession.RemoveObjectSpace((IObjectSpace)sender); };
        }

        public static bool IsNotRefreshing(this IObjectSpace objectSpace)
        {
            return CoreService.NotRefreshingSession.IsNotRefreshing(objectSpace);
        }

        public static void DisableNotRefreshing(this IObjectSpace objectSpace)
        {
            CoreService.NotRefreshingSession.RemoveObjectSpace(objectSpace);
        }
         */
    }
}
