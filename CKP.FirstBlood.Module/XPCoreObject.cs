using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Linq.Expressions;
using CKP.FirstBlood.Module.Common;


namespace CKP.FirstBlood.Module.BusinessObjects
{
    [NonPersistent]
    public abstract class XPCoreObject : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public XPCoreObject(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

       

        public virtual void MemberHasBeenChanged(string memberName)
        {
            OnChanged(memberName);
        }

        protected internal T SafeGet<T>(Func<T> factory, T ifException, Func<T, T> fitting)
        {
            try
            {
                T res = factory.Invoke();
                if (fitting != null) return fitting.Invoke(res);
                else return res;
            }
            catch (Exception ex)
            {
                return ifException;
            }
        }

        public bool SetPropertyValue(string propertyName, ref string propertyValueHolder, string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                newValue = null;
            else
                newValue = newValue.Trim();

            return base.SetPropertyValue<string>(propertyName, ref propertyValueHolder, newValue);
        }

        public bool SetPropertyValueWithEmpty(string propertyName, ref string propertyValueHolder, string newValue)
        {
            if (newValue == null) newValue = string.Empty;
            return base.SetPropertyValue<string>(propertyName, ref propertyValueHolder, newValue);
        }


        public decimal? EvaluateAliasDecimalNullable(string memberName)
        {
            object val = EvaluateAlias(memberName);
            if (val == null) return null;
            else return Convert.ToDecimal(EvaluateAlias(memberName));
        }

        public DateTime? EvaluateAliasDateTimeNullable(string memberName)
        {
            object val = EvaluateAlias(memberName);
            if (val == null) return null;
            else return Convert.ToDateTime(EvaluateAlias(memberName));
        }

        public T EvaluateAliasNotNull<T>(string memberName, Func<object, T> converter, T nullValue)
        {
            object val = EvaluateAlias(memberName);
            if (val == null) return nullValue;
            else return converter(val);
        }



        [Browsable(false)]
        public virtual bool IsLoadingOrServerSide
        {
            get { return IsLoading; }
        }


        public T EvaluateAliasWithConv<T>(string memberName, T ifNull)
        {
            object obj = EvaluateAlias(memberName);

            if (obj == null) return ifNull;
            else return (T)obj;
        }


        public propertyType EvaluateAlias<T, propertyType>(Expression<Func<T, propertyType>> propertyLambda, propertyType valueIfNull) where T : XPCoreObject
        {
            object value = EvaluateAlias(ForReflectionHelper.GetPropertyInfo<T, propertyType>(this as T, propertyLambda).Name);
            if (value == null) return valueIfNull;
            return (propertyType)value;
        }

        public string EvaluateAlias<T>(Expression<Func<T, string>> propertyLambda, string valueIfNull = null) where T : XPCoreObject
        {
            string result = EvaluateAlias<T, string>(propertyLambda, valueIfNull);
            return result;
        }


        public bool SafeCond(bool ifException, Func<bool> func)
        {
            try
            {
                return func.Invoke();
            }
            catch
            {
                return ifException;
            }
        }
        
    }



    public enum XpTakNieNic
    {
        NieUstawione = 0, // nie rusza� cyfr!
        Tak = 1,
        Nie = 2
    }




}
