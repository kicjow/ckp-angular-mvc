using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
   
    [DefaultClassOptions]
    [NavigationItem("Administracja")]
    [DefaultProperty("Nazwa")]
    [System.ComponentModel.DisplayName("Jednostka")]
    public class JednostkaOrg : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public JednostkaOrg(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        // Fields... 
        private string _Email;
        private string _Telefony;        
        private string _Adres1;
        private string _Nazwa;
        private string _Kod;
        

        [RuleUniqueValue(DefaultContexts.Save)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Kod
        {
            get { return _Kod; }
            set { SetPropertyValue("Kod", ref _Kod, value); }
        }

        [Size(200)]
        public string Nazwa
        {
            get { return _Nazwa; }
            set { SetPropertyValue("Nazwa", ref _Nazwa, value); }
        }


        [Size(500)]
        public string Adres1
        {
            get { return _Adres1; }
            set { SetPropertyValue("Adres1", ref _Adres1, value); }
        }



        public string Telefony
        {
            get { return _Telefony; }
            set { SetPropertyValue("Telefony", ref _Telefony, value); }
        }



        public string Email
        {
            get { return _Email; }
            set { SetPropertyValue("Email", ref _Email, value); }
        }
    }
}
