using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo.Metadata;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    public class DaneOsobowe : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public DaneOsobowe(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        private Obywatelstwo _Obywatelstwo;
       private string _Salt;
        private string _Poczta;
        private bool _Newsletter;
        private string _NumerPrawaWykonywaniaZawodu;
        private string _Telefon;
        private string _Haslo;
        private RodzajDokumentu _RodzajDokumentu;
        private string _DokumentNumer;
        private P�e� _P�e�;
        private DateTime _DataUrodzenia;
        private DateTime? _PlanownyTerminZakSpecjaliz;
        private DateTime? _RozpSpecjalizacji;
        private Specjalizacja _Specjalizacja;
        private string _Email;
        private string _Ulica;
        private string _Miejscowo��;
        private string _KodPocztowy;
        private string _Nazwisko;
        private string _Imi�2;
        private string _Imi�;
        private bool _NieposiadaPESEL;
        private string _PESEL;


        public Obywatelstwo Obywatelstwo
        {
            get
            {
                return _Obywatelstwo;
            }
            set
            {
                SetPropertyValue("Obywatelstwo", ref _Obywatelstwo, value);
            }
        }

        [RuleRequiredField(TargetCriteria = "NieposiadaPESEL == false", CustomMessageTemplate = "PESEL nie mo�e by� pusty")]
        //[RuleCriteria("Len(PESEL) != 11", TargetCriteria = "NieposiadaPESEL == false", CustomMessageTemplate = "Z�a d�ugo�� PESEL")]
        public string PESEL
        {
            get { return _PESEL; }
            set { SetPropertyValue("PESEL", ref _PESEL, value); }
        }

        // typ dokumentu
        public bool NieposiadaPESEL
        {
            get { return _NieposiadaPESEL; }
            set { SetPropertyValue("NieposiadaPESEL", ref _NieposiadaPESEL, value); }
        }


        public string Imi�
        {
            get { return _Imi�; }
            set { SetPropertyValue("Imi�", ref _Imi�, value); }
        }


        public string Imi�2
        {
            get { return _Imi�2; }
            set { SetPropertyValue("Imi�2", ref _Imi�2, value); }
        }


        public string Nazwisko
        {
            get { return _Nazwisko; }
            set { SetPropertyValue("Nazwisko", ref _Nazwisko, value); }
        }


        public string KodPocztowy
        {
            get { return _KodPocztowy; }
            set { SetPropertyValue("KodPocztowy", ref _KodPocztowy, value); }
        }


        public string Miejscowo��
        {
            get { return _Miejscowo��; }
            set { SetPropertyValue("Miejscowo��", ref _Miejscowo��, value); }
        }

        [Size(200)]
        public string Ulica
        {
            get { return _Ulica; }
            set { SetPropertyValue("Ulica", ref _Ulica, value); }
        }

        public string Poczta
        {
            get
            {
                return _Poczta;
            }
            set
            {
                SetPropertyValue("Poczta", ref _Poczta, value);
            }
        }

        public string Telefon
        {
            get { return _Telefon; }
            set { SetPropertyValue("Telefon", ref _Telefon, value); }
        }


        public string Email
        {
            get { return _Email; }
            set { SetPropertyValue("Email", ref _Email, value); }
        }


        public Specjalizacja Specjalizacja
        {
            get { return _Specjalizacja; }
            set { SetPropertyValue("Specjalizacja", ref _Specjalizacja, value); }
        }



        public DateTime? RozpSpecjalizacji
        {
            get { return _RozpSpecjalizacji; }
            set { SetPropertyValue("RozpSpecjalizacji", ref _RozpSpecjalizacji, value); }
        }


        [ToolTip("Planowany termin zako�czenia specjalizacji")]
        public DateTime? PlanownyTerminZakSpecjaliz
        {
            get { return _PlanownyTerminZakSpecjaliz; }
            set { SetPropertyValue("PlanownyTerminZakSpecjaliz", ref _PlanownyTerminZakSpecjaliz, value); }
        }



        public DateTime DataUrodzenia
        {
            get
            {
                return _DataUrodzenia;
            }
            set
            {
                SetPropertyValue("DataUrodzenia", ref _DataUrodzenia, value);
            }
        }


        public P�e� P�e�
        {
            get
            {
                return _P�e�;
            }
            set
            {
                SetPropertyValue("P�e�", ref _P�e�, value);
            }
        }

        [Browsable(false)]
        public string DokumentNumer
        {
            get
            {
                return _DokumentNumer;
            }
            set
            {
                SetPropertyValue("DokumentNumer", ref _DokumentNumer, value);
            }
        }

        [Browsable(false)]
        public RodzajDokumentu RodzajDokumentu
        {
            get
            {
                return _RodzajDokumentu;
            }
            set
            {
                SetPropertyValue("RodzajDokumentu", ref _RodzajDokumentu, value);
            }
        }


        public string Haslo
        {
            get
            {
                return _Haslo;
            }
            set
            {
                SetPropertyValue("Haslo", ref _Haslo, value);
            }
        }

        public string NumerPrawaWykonywaniaZawodu
        {
            get
            {
                return _NumerPrawaWykonywaniaZawodu;
            }
            set
            {
                SetPropertyValue("NumerPrawaWykonywaniaZawodu", ref _NumerPrawaWykonywaniaZawodu, value);
            }
        }


        public bool Newsletter
        {
            get
            {
                return _Newsletter;
            }
            set
            {
                SetPropertyValue("Newsletter", ref _Newsletter, value);
            }
        }

        public void CopyTo(DaneOsobowe daneOsobowe)
        {
            foreach (XPMemberInfo pPropes in this.ClassInfo.PersistentProperties)
            {
                pPropes.SetValue(daneOsobowe, pPropes.GetValue(this));
            }
            if (System.Diagnostics.Debugger.IsAttached)
            {
                if (ClassInfo.CollectionProperties.Cast<XPMemberInfo>().FirstOrDefault() != null)
                    throw new NotImplementedException();
            }
        }


        public string Salt
        {
            get
            {
                return _Salt;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(_Salt))
                {
                    SetPropertyValue("Salt", ref _Salt, value);
                }
                else
                {
                    //TODO jakis throw? dunno
                }

            }
        }


    }
}
