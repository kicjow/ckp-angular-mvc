using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using System.Data;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    [Browsable(false)]
    public class EmailResetDataContainer : BaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public EmailResetDataContainer(Session session)
            : base(session)
        {

        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (http://documentation.devexpress.com/#Xaf/CustomDocument2619).
        //    this.PersistentProperty = "Paid";
        //}

        // Fields...

        private bool _IsUsed;
        private DateTime? _RequestCreationDateTime;
        private string _ClientEmail;
        private string _ClientLogin;
        private string _ClientIP;

        public string ClientIP
        {
            get
            {
                return _ClientIP;
            }
            set
            {
                SetPropertyValue("ClientIP", ref _ClientIP, value);
            }
        }


        public string ClientLogin
        {
            get
            {
                return _ClientLogin;
            }
            set
            {
                SetPropertyValue("ClientLogin", ref _ClientLogin, value);
            }
        }


        public string ClientEmail
        {
            get
            {
                return _ClientEmail;
            }
            set
            {
                SetPropertyValue("ClientEmail", ref _ClientEmail, value);
            }
        }


        public DateTime? RequestCreationDateTime
        {
            get
            {
                return _RequestCreationDateTime;
            }
            set
            {
                SetPropertyValue("RequestCreationDateTime", ref _RequestCreationDateTime, value);
            }
        }


        public bool IsUsed
        {
            get
            {
                return _IsUsed;
            }
            set
            {
                SetPropertyValue("IsUsed", ref _IsUsed, value);
            }
        }

    }
}
