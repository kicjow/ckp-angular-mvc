using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Administracja")]
    [RuleCombinationOfPropertiesIsUnique("Kurs-LpKursuRok-Uniq", DefaultContexts.Save, "LpKursu,Rok")]
    public class Kurs : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public Kurs(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Rok = 2015;
        }


        // Fields...        
        private KierownikKursu _Kierownik;
        private int _LpKursu;        
        private MiejsceKursu _Miejsce;
        private TypyKursów _TypKursu;
        private string _Nazwa;
        private JednostkaOrg _Jednostka;
        private Specjalizacja _Specjalizacja;
        private int _Rok;




        public string NumerKursu
        {
            get { return string.Format("AKP/S{0}/{1}", LpKursu, Rok); }
        }


        [RuleUniqueValue]
        [RuleRequiredField]
        public int LpKursu
        {
            get { return _LpKursu; }
            set { SetPropertyValue("LpKursu", ref _LpKursu, value); }
        }

        [RuleUniqueValue(DefaultContexts.Save)]        
        [RuleRequiredField(DefaultContexts.Save)]
        [Size(500)]
        public string Nazwa
        {
            get { return _Nazwa; }
            set { SetPropertyValue("Nazwa", ref _Nazwa, value); }
        }

        public int Rok
        {
            get { return _Rok; }
            set { SetPropertyValue("Rok", ref _Rok, value); }
        }

        public Specjalizacja Specjalizacja
        {
            get { return _Specjalizacja; }
            set { SetPropertyValue("Specjalizacja", ref _Specjalizacja, value); }
        }

        public JednostkaOrg Jednostka
        {
            get { return _Jednostka; }
            set { SetPropertyValue("Jednostka", ref _Jednostka, value); }
        }


        [Association("Kurs-Terminy"), Aggregated]
        public XPCollection<TerminKursu> Terminy
        {
            get { return GetCollection<TerminKursu>("Terminy"); }
        }


        public TypyKursów TypKursu
        {
            get { return _TypKursu; }
            set { SetPropertyValue("TypKursu", ref _TypKursu, value); }
        }

        public MiejsceKursu Miejsce
        {
            get { return _Miejsce; }
            set { SetPropertyValue("Miejsce", ref _Miejsce, value); }
        }

        [Association]
        public KierownikKursu Kierownik
        {
            get
            {
                return _Kierownik;
            }
            set
            {
                SetPropertyValue("Kierownik", ref _Kierownik, value);
            }
        }
        
    }

    public enum TypyKursów
    {
        Specjalizacyjne = 0,
        Doskonalące = 1,
        Ustawiczne = 2
    }


    public enum MiejsceKursu
    {
        Dziekanat = 0,
        Klinika = 1
    }
}
