﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace CKP.FirstBlood.Module.BusinessObjects.EmailProcessor
{
    public class EmailProcessor : IEmailProcessor
    {
        public void ProcessEmail(EmailSetting setings, EmailContent content)
        {
            //try
            //{
            //    MailMessage mail = new MailMessage(setings.MailFromAdress, setings.MeilToAdress);
            //    mail.From = new MailAddress(setings.MailFromAdress); // dunno, lel
            //    mail.Subject = content.Title;
            //    mail.Body = content.Body;
            //    mail.IsBodyHtml = setings.IsBodyHtml;
            //    mail.BodyEncoding = setings.BodyEncoding;
            //    mail.SubjectEncoding = setings.SubjectEncoding;
            //    using (var smtp = new SmtpClient())
            //    {
            //        smtp.Host = setings.ServerName;
            //        smtp.Credentials = new NetworkCredential(setings.MailFromAdress, setings.Password);
            //        smtp.EnableSsl = setings.UseSsl;
            //        smtp.Send(mail);
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}

            SmtpClient klientSMTP = new SmtpClient();
            klientSMTP.Host = "mx01.am.edu.pl";
            klientSMTP.UseDefaultCredentials = true;

            try
            {
                content.Body = string.Format(
                    "By zmienić hasło służące do logowania się na stronie rejestracji ckp.wum.edu.pl proszę kliknąć link: {0}",
                    content.LinkDoZmiany);

                MailMessage wiadomosc = new MailMessage("<recrutment_noreply@wum.edu.pl>",
                    setings.MeilToAdress, 
                    content.Title,
                    content.Body);

                wiadomosc.IsBodyHtml = true;
                klientSMTP.Send(wiadomosc);
            }
            catch { }


        }
    }
}
