﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKP.FirstBlood.Module.BusinessObjects.EmailProcessor
{
    public class EmailSetting
    {
        public string MeilToAdress;
        public string MailFromAdress;
        public bool UseSsl;
        public string Username;
        public string Password;
        public string ServerPort;
        public string ServerName;
        public bool writeasFile;
        public bool IsBodyHtml;
        public Encoding BodyEncoding = Encoding.UTF8;
        public Encoding SubjectEncoding = Encoding.UTF8;
    }
}
