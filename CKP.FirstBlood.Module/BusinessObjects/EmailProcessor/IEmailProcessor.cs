﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKP.FirstBlood.Module.BusinessObjects.EmailProcessor
{
    public interface IEmailProcessor
    {
        void ProcessEmail(EmailSetting setings, EmailContent content);
    }
}
