using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("S�owniki")]
    public class Specjalizacja : DictBase
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public Specjalizacja(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        // Fields...
        private RodzajSpecjalnosci _RodzajSpecjalonosci;

        public RodzajSpecjalnosci RodzajSpecjalonosci
        {
            get
            {
                return _RodzajSpecjalonosci;
            }
            set
            {
                SetPropertyValue("RodzajSpecjalonosci", ref _RodzajSpecjalonosci, value);
            }
        }
    }
}
