using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using CKP.FirstBlood.Module.Common;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kandydaci/Aplikacje")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    public class Aplikacja : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public Aplikacja(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DaneOsobowe = new DaneOsobowe(Session);
        }



        // Fields...
        private DaneDoFaktury _DaneDoFaktury;
        private Kandydat _Kandydat;
        private TerminKursu _Termin;
        private DaneOsobowe _DaneOsobowe;
        private bool _PotwierdzenieOswiadczenia;
        private DateTime? _PlanowanyTerminZakonczeniaSpecjalizacji;
        private DateTime? _DataRozpoczeciaSpecjalizacji;

        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never)]
        public DaneOsobowe DaneOsobowe
        {
            get { return _DaneOsobowe; }
            set { SetPropertyValue("DaneOsobowe", ref _DaneOsobowe, value); }
        }

        [Association("Kandydat-Aplikacje")]
        public Kandydat Kandydat
        {
            get { return _Kandydat; }
            set { SetPropertyValue("Kandydat", ref _Kandydat, value); }
        }

        [Association("TerminKursu-Aplikacje")]
        public TerminKursu Termin
        {
            get { return _Termin; }
            set { SetPropertyValue("Termin", ref _Termin, value); }
        }

        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never)]
        public DaneDoFaktury DaneDoFaktury
        {
            get { return _DaneDoFaktury; }
            set { SetPropertyValue("DaneDoFaktury", ref _DaneDoFaktury, value); }
        }

        public DateTime? DataRozpoczeciaSpecjalizacji
        {
            get
            {
                return _DataRozpoczeciaSpecjalizacji;
            }
            set
            {
                SetPropertyValue("DataRozpoczeciaSpecjalizacji", ref _DataRozpoczeciaSpecjalizacji, value);
            }
        }

        public DateTime? PlanowanyTerminZakonczeniaSpecjalizacji
        {
            get
            {
                return _PlanowanyTerminZakonczeniaSpecjalizacji;
            }
            set
            {
                SetPropertyValue("PlanowanyTerminZakonczeniaSpecjalizacji", ref _PlanowanyTerminZakonczeniaSpecjalizacji, value);
            }
        }

        public bool PotwierdzenieOswiadczenia
        {
            get
            {
                return _PotwierdzenieOswiadczenia;
            }
            set
            {
                SetPropertyValue("PotwierdzenieOswiadczenia", ref _PotwierdzenieOswiadczenia, value);
            }
        }

        public bool Wypelniona
        {
            get
            {
                return SaWypelnione();
            }
        }

        private bool SaWypelnione()
        {
            if (this.Termin == null || Termin.Kurs == null || DaneDoFaktury == null)
            {
                return false; //zrobilem to zeby sprawdzic czy te wartoscie nie s� nulowe w jednej liniejce. gdybym dal == zamiast != i zwracal false w tym miejsce gdize ten komentarz to by moglo sie wywalic ju� w linijce 
            }


            var polaString = new List<string>()
            {
                this.Termin.Kurs.Nazwa,
                this.DaneOsobowe.Imi�,
                this.DaneOsobowe.Nazwisko,
                this.DaneOsobowe.Miejscowo��,
                this.DaneOsobowe.Ulica,
                this.DaneOsobowe.KodPocztowy,
                this.DaneOsobowe.Poczta,
                this.DaneOsobowe.Telefon,
                this.DaneOsobowe.Email

            };

            var polaDateTime = new List<DateTime?>()
            {
                this.Termin.Pocz�tek,
                this.Termin.Koniec
            };

            foreach (var item in polaString)
            {
                if (string.IsNullOrWhiteSpace(item))
                {
                    return false;
                }
            }

            foreach (var item in polaDateTime)
            {
                if (!item.HasValue)
                {
                    return false;
                }
            }


            var polaFakturaString = new List<string>()
            {
                this.DaneDoFaktury.KodPocztowy,
                this.DaneDoFaktury.Miejscowo��,
                this.DaneDoFaktury.Ulica,
                this.DaneDoFaktury.NIP
            };

            if (KursHelper.SprawdzCzyMaBycWidocznaFaktura(this))
            {
                foreach (var item in polaFakturaString)
                {
                    if (string.IsNullOrWhiteSpace(item))
                    {
                        return false;
                    }
                }
            }
          

            return true;
        }
    }
}
