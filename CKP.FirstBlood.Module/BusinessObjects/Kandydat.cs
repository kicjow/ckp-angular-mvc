using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("Kandydaci/Aplikacje")]
    public class Kandydat : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public Kandydat(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            DaneOsobowe = new DaneOsobowe(Session);
        }


        // Fields...                

        // Fields...
      
        private DaneOsobowe _DaneOsobowe;
        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.InDetailView)]
        public DaneOsobowe DaneOsobowe
        {
            get { return _DaneOsobowe; }
            set { SetPropertyValue("DaneOsobowe", ref _DaneOsobowe, value); }
        }


        [Association("Kandydat-Aplikacje"), Aggregated]
        public XPCollection<Aplikacja> Aplikacje
        {
            get { return GetCollection<Aplikacja>("Aplikacje"); }
        }

        [Association("Kandydat-DaneDoFaktury"), Aggregated]
        public XPCollection<KandydatDaneDoFaktury> DaneDoFaktury
        {
            get { return GetCollection<KandydatDaneDoFaktury>("DaneDoFaktury"); }
        }
    }
}
