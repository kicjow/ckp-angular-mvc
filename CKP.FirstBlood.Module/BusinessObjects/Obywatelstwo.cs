using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [DefaultClassOptions]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
        [NavigationItem("S�owniki")]
    public class Obywatelstwo : DictBase
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public Obywatelstwo(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (http://documentation.devexpress.com/#Xaf/CustomDocument2619).
        //    this.PersistentProperty = "Paid";
        //}
        static internal void CreateDefault(IObjectSpace os)
        {
            string dictPefix = "dObyw_";
            Type type = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

            #region dlugie
            Dictionary<string, string> toInsert = new Dictionary<string, string>()
            {
                {"PL","polskie"},
                {"00","apartyda (bezpa�stwowiec)"},
                {"AF","afga�skie"},
                {"AL","alba�skie"},
                {"DZ","algierskie"},
                {"US","ameryka�skie"},
                {"UM","ameryka�skie (Dalekich Mniejszych Wysp Stan�w Zjednoczonych)"},
                {"GU","ameryka�skie (guamskie)"},
                {"PR","ameryka�skie (portoryka�skie)"},
                {"MP","ameryka�skie (p�nocnomaria�skie)"},
                {"AS","ameryka�skie (Samoa Ameryka�skiego)"},
                {"VI","ameryka�skie (Wysp Dziewiczych Stan�w Zjednoczonych)"},
                {"AD","andorskie"},
                {"AO","angolskie"},
                {"AQ","Antarktyka"},
                {"AG","Antiguy i Barbudy"},
                {"AR","argenty�skie"},
                {"AM","arme�skie"},
                {"AU","australijskie"},
                {"NF","australijskie (norfolskie)"},
                {"HM","australijskie (Wysp Heard i McDonalda)"},
                {"CC","australijskie (Wysp Kokosowych)"},
                {"CX","australijskie (Wyspy Bo�ego Narodzenia)"},
                {"AT","austriackie"},
                {"AZ","azerbejd�a�skie"},
                {"BS","bahamskie"},
                {"BH","bahraj�skie"},
                {"BD","bangladeskie"},
                {"BB","barbadoskie"},
                {"BE","belgijskie"},
                {"BZ","belize�skie"},
                {"BJ","beni�skie"},
                {"BT","bhuta�skie"},
                {"BY","bia�oruskie"},
                {"MM","birma�skie"},
                {"BO","boliwijskie"},
                {"BA","bo�niacko-hercegowi�skie"},
                {"BW","botswa�skie"},
                {"BR","brazylijskie"},
                {"BN","brunejskie"},
                {"GB","brytyjskie"},
                {"AI","brytyjskie (Anguilli)"},
                {"BM","brytyjskie (bermudzkie)"},
                {"VG","brytyjskie (Brytyjskich Wysp Dziewiczych)"},
                {"IO","brytyjskie (Brytyjskiego Terytorium Oceanu Indyjskiego)"},
                {"FK","brytyjskie (falklandzkie)"},
                {"GI","brytyjskie (gibraltarskie)"},
                {"GG","brytyjskie (Guernsey)"},
                {"JE","brytyjskie (Jersey)"},
                {"KY","brytyjskie (kajma�skie)"},
                {"MS","brytyjskie (montserrackie)"},
                {"GS","brytyjskie (Wysp Georgia Po�udniowa i Sandwich Po�udniowy)"},
                {"TC","brytyjskie (Wysp Turks i Caicos)"},
                {"IM","brytyjskie (Wyspy Man)"},
                {"PN","brytyjskie (Wyspy Pitcairn)"},
                {"SH","brytyjskie (Wyspy �wi�tej Heleny)"},
                {"BG","bu�garskie"},
                {"BF","burki�skie"},
                {"BI","burundyjskie"},
                {"CL","chilijskie"},
                {"CN","chi�skie"},
                {"HK","chi�skie (hongko�skie)"},
                {"MO","chi�skie (Makau)"},
                {"TW","chi�skie (tajwa�skie)"},
                {"HR","chorwackie"},
                {"CY","cypryjskie"},
                {"TD","czadyjskie"},
                {"ME","czarnog�rskie"},
                {"CZ","czeskie"},
                {"DO","dominika�skie"},
                {"DM","Dominiki"},
                {"DK","du�skie"},
                {"GL","du�skie (grenlandzkie)"},
                {"FO","du�skie (Wysp Owczych)"},
                {"DJ","d�ibutyjskie"},
                {"EG","egipskie"},
                {"EC","ekwadorskie"},
                {"AE","emirackie"},
                {"ER","erytrejskie"},
                {"EE","esto�skie"},
                {"ET","etiopskie"},
                {"FJ","fid�ijskie"},
                {"PH","filipi�skie"},
                {"FI","fi�skie"},
                {"AX","fi�skie (alandzkie)"},
                {"FR","francuskie"},
                {"TF","francuskie (Francuskich Terytori�w Po�udniowych)"},
                {"GP","francuskie (gwadelupskie)"},
                {"YT","francuskie (majotyjskie)"},
                {"MQ","francuskie (martynika�skie)"},
                {"NC","francuskie (nowokaledo�skie)"},
                {"PF","francuskie (polinezyjskie)"},
                {"RE","francuskie (reunio�skie)"},
                {"BL","francuskie (Saint Barth�lemy)"},
                {"MF","francuskie (Saint Martin)"},
                {"PM","francuskie (Saint Pierre i Miquelon)"},
                {"WF","francuskie (Wysp Wallis i Futuna)"},
                {"GF","francusko-guja�skie"},
                {"GA","gabo�skie"},
                {"GM","gambijskie"},
                {"GH","gha�skie"},
                {"GR","greckie"},
                {"GD","grenadyjskie"},
                {"GE","gruzi�skie"},
                {"GY","guja�skie"},
                {"GT","gwatemalskie"},
                {"GN","gwinejskie"},
                {"GW","gwinejskie (Gwinea Bissau)"},
                {"GQ","gwinejskie (Gwinea R�wnikowa)"},
                {"HT","haita�skie"},
                {"ES","hiszpa�skie"},
                {"CW","holenderskie"},
                {"NL","holenderskie"},
                {"AW","holenderskie (aruba�skie)"},
                {"BQ","holenderskie (Bonaire, Sint Eustatius i Saba)"},
                {"SX","holenderskie (Sint Maarten)"},
                {"HN","honduraskie"},
                {"ID","indonezyjskie"},
                {"IN","indyjskie"},
                {"IQ","irakijskie"},
                {"IR","ira�skie"},
                {"IE","irlandzkie"},
                {"IS","islandzkie"},
                {"CI","iworyjskie"},
                {"IL","izraelskie"},
                {"JM","jamajskie"},
                {"JP","japo�skie"},
                {"YE","jeme�skie"},
                {"JO","jorda�skie"},
                {"CV","kabowerdy�skie"},
                {"KH","kambod�a�skie"},
                {"CM","kameru�skie"},
                {"CA","kanadyjskie"},
                {"QA","katarskie"},
                {"KZ","kazachskie"},
                {"KE","kenijskie"},
                {"KG","kirgiskie"},
                {"KI","kiribatyjskie"},
                {"CO","kolumbijskie"},
                {"KM","komoryjskie"},
                {"CD","kongijskie (Demokratyczna Republika)"},
                {"CG","kongijskie (Republika)"},
                {"XK","kosowskie"},
                {"CR","kostaryka�skie"},
                {"CU","kuba�skie"},
                {"KW","kuwejckie"},
                {"LK","lankijskie"},
                {"LA","laota�skie"},
                {"LS","lesotyjskie"},
                {"LB","liba�skie"},
                {"LR","liberyjskie"},
                {"LY","libijskie"},
                {"LI","liechtenstei�skie"},
                {"LT","litewskie"},
                {"LU","luksemburskie"},
                {"LV","�otewskie"},
                {"MK","macedo�skie"},
                {"MG","madagaskarskie"},
                {"MW","malawijskie"},
                {"MV","malediwskie"},
                {"MY","malezyjskie"},
                {"ML","malijskie"},
                {"MT","malta�skie"},
                {"MA","maroka�skie"},
                {"MH","marszalskie"},
                {"MR","maureta�skie"},
                {"MU","maurytyjskie"},
                {"MX","meksyka�skie"},
                {"FM","mikronezyjskie"},
                {"MD","mo�dawskie"},
                {"MC","monakijskie"},
                {"MN","mongolskie"},
                {"MZ","mozambickie"},
                {"NA","namibijskie"},
                {"NR","naurua�skie"},
                {"NP","nepalskie"},
                {"DE","niemieckie"},
                {"NE","nigerskie"},
                {"NG","nigeryjskie"},
                {"NI","nikaragua�skie"},
                {"NO","norweskie"},
                {"BV","norweskie (Wyspy Bouveta)"},
                {"NZ","nowozelandzkie"},
                {"NU","nowozelandzkie (niue�skie)"},
                {"TK","nowozelandzkie (Tokelau)"},
                {"CK","nowozelandzkie (Wysp Cooka)"},
                {"OM","oma�skie"},
                {"PK","pakista�skie"},
                {"PW","Palau"},
                {"PS","palesty�skie"},
                {"PA","panamskie"},
                {"PG","papua�skie"},
                {"PY","paragwajskie"},
                {"PE","peruwia�skie"},
                {"ZA","po�udniowoafryka�skie"},
                {"KR","po�udniowokorea�skie"},
                {"SS","po�udniowosuda�skie"},
                {"PT","portugalskie"},
                {"KP","p�nocnokorea�skie"},
                {"RU","rosyjskie"},
                {"RO","rumu�skie"},
                {"RW","rwandyjskie"},
                {"KN","Saint Kitts i Nevis"},
                {"LC","Saint Lucia"},
                {"VC","Saint Vincent i Grenadyn�w"},
                {"SB","salomo�skie"},
                {"SV","salwadorskie"},
                {"WS","samoa�skie"},
                {"SM","sanmary�skie"},
                {"SA","saudyjskie"},
                {"SN","senegalskie"},
                {"RS","serbskie"},
                {"SC","seszelskie"},
                {"SL","sierraleo�skie"},
                {"SG","singapurskie"},
                {"SK","s�owackie"},
                {"SI","s�owe�skie"},
                {"SO","somalijskie"},
                {"SZ","suazyjskie"},
                {"SD","suda�skie"},
                {"SR","surinamskie"},
                {"SY","syryjskie"},
                {"CH","szwajcarskie"},
                {"SE","szwedzkie"},
                {"CF","�rodkowoafryka�skie"},
                {"TJ","tad�yckie"},
                {"TH","tajlandzkie"},
                {"TZ","tanza�skie"},
                {"TG","togijskie"},
                {"TO","tongijskie"},
                {"TT","trynidadzko-tobagijskie"},
                {"TN","tunezyjskie"},
                {"TR","tureckie"},
                {"TM","turkme�skie"},
                {"TV","tuwalskie"},
                {"UG","ugandyjskie"},
                {"UA","ukrai�skie"},
                {"UY","urugwajskie"},
                {"UZ","uzbeckie"},
                {"VU","wanuackie"},
                {"VA","watyka�skie"},
                {"VE","wenezuelskie"},
                {"HU","w�gierskie"},
                {"VN","wietnamskie"},
                {"IT","w�oskie"},
                {"TL","wschodniotimorskie"},
                {"ST","Wysp �wi�tego Tomasza i Ksi���cej"},
                {"EH","zachodniosaharyjskie"},
                {"ZM","zambijskie"},
                {"ZW","Zimbabwe"}
            };
            #endregion
            foreach (var kv in toInsert)
            {
                string kod = dictPefix + kv.Key;
                DictBase dict = os.FindObject(type, CriteriaOperator.Parse("Kod = ?", kod)) as DictBase;
                if (dict == null)
                {
                    dict = (DictBase)os.CreateObject(type);
                    dict.Kod = kod;
                    dict.Nazwa = kv.Value;
                }
            }
        }
    }
}
