using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{    
    [Persistent]
    public class DaneDoFaktury : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public DaneDoFaktury(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        // Fields...
        private string _NIP;
        private string _Nazwa;
        private string _Ulica;
        private string _Miejscowość;
        private string _KodPocztowy;


        [Size(500)]
        public string Nazwa
        {
            get { return _Nazwa; }
            set { SetPropertyValue("Nazwa", ref _Nazwa, value); }
        }


        public string KodPocztowy
        {
            get { return _KodPocztowy; }
            set { SetPropertyValue("KodPocztowy", ref _KodPocztowy, value); }
        }


        public string Miejscowość
        {
            get { return _Miejscowość; }
            set { SetPropertyValue("Miejscowość", ref _Miejscowość, value); }
        }

        [Size(200)]
        public string Ulica
        {
            get { return _Ulica; }
            set { SetPropertyValue("Ulica", ref _Ulica, value); }
        }

        public string NIP
        {
            get { return _NIP; }
            set { SetPropertyValue("NIP", ref _NIP, value); }
        }



        public void Copy(DaneDoFaktury src)
        {
            
        }
    }
}
