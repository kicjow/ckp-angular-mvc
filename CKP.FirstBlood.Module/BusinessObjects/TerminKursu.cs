using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using dc = DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    
    [dc.XafDefaultProperty("Pocz�tek")]
    [Persistent]
    public class TerminKursu : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public TerminKursu(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            
        }

        // Fields...
        private bool _RejestracjaOtwarta;
        private int _Numer;
        private Kurs _Kurs;
        private DateTime? _Koniec;
        private DateTime? _Pocz�tek;
        
       [Association("Kurs-Terminy")]
        public Kurs Kurs
        {
            get { return _Kurs; }
            set { SetPropertyValue("Kurs", ref _Kurs, value); }
        }


       public bool RejestracjaOtwarta
       {
           get
           {
               return _RejestracjaOtwarta;
           }
           set
           {
               SetPropertyValue("RejestracjaOtwarta", ref _RejestracjaOtwarta, value);
           }
       }


        [RuleRequiredField(DefaultContexts.Save)]
        public DateTime? Pocz�tek
        {
            get { return _Pocz�tek; }
            set 
            { 
                SetPropertyValue("Pocz�tek", ref _Pocz�tek, value);
                if (!IsLoading && Koniec == null) Koniec = Pocz�tek;            
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public DateTime? Koniec
        {
            get { return _Koniec; }
            set { SetPropertyValue("Koniec", ref _Koniec, value); }
        }


        public int Numer
        {
            get { return _Numer; }
            set { SetPropertyValue("Numer", ref _Numer, value); }
        }




        [Association("TerminKursu-Aplikacje")]
        public XPCollection<Aplikacja> Aplikacje
        {
            get { return GetCollection<Aplikacja>("Aplikacje"); }
        }
    }
}
