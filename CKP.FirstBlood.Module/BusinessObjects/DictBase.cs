using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using CKP.FirstBlood.Module;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [NonPersistent]
    //[NavigationItem("S�owniki")]
    //[System.ComponentModel.DisplayName("Wszystkie s�owniki")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (http://documentation.devexpress.com/#Xaf/CustomDocument2701).
    public abstract class DictBase : XPCoreObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public DictBase(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (http://documentation.devexpress.com/#Xaf/CustomDocument2619).
        //    this.PersistentProperty = "Paid";
        //}

        // Fields...
        private string _Nazwa;
        private string _Kod;

        [VisibleInListView(true)]
        [VisibleInLookupListView(true)]
        [RuleRequiredField()]
        [RuleUniqueValue()]
        public string Kod
        {
            get
            {
                return _Kod;
            }
            set
            {
                SetPropertyValue("Kod", ref _Kod, value);
            }
        }

        [VisibleInListView(true)]
        [VisibleInLookupListView(true)]
        [RuleRequiredField()]
        public string Nazwa
        {
            get
            {
                return _Nazwa;
            }
            set
            {
                SetPropertyValue("Nazwa", ref _Nazwa, value);
            }
        }

        public static T FromCode<T>(string code, IObjectSpace os) where T :DictBase
        {
            return os.FindObject<T>(x => x.Kod == code);
        }

        public override string ToString()
        {
            return Nazwa;
        }
    }
}
