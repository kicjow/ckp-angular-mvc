using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace CKP.FirstBlood.Module.BusinessObjects
{
    [Persistent]    
    public class KandydatDaneDoFaktury : DaneDoFaktury
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (http://documentation.devexpress.com/#Xaf/CustomDocument3146).
        public KandydatDaneDoFaktury(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        // Fields...
        private Kandydat _OryginalnyWlasciciel;
        private Kandydat _Kandydat;
        [Association("Kandydat-DaneDoFaktury")]
        public Kandydat Kandydat
        {
            get { return _Kandydat; }
            set
            { 
                SetPropertyValue("Kandydat", ref _Kandydat, value); 
                if (value != null)
                {
                    OryginalnyWlasciciel = value;
                }
            }
        }


        public Kandydat OryginalnyWlasciciel
        {
            get
            {
                return _OryginalnyWlasciciel;
            }
            set
            {
                SetPropertyValue("OryginalnyWlasciciel", ref _OryginalnyWlasciciel, value);
            }
        }
    }
}
