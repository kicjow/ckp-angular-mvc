﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace CKP.FirstBlood.Module.Common
{
    public static class ForReflectionHelper
    {
        public static PropertyInfo GetPropertyInfo<TSource, TProperty>(
     TSource source,
     Expression<Func<TSource, TProperty>> propertyLambda)
        {
            Type type = typeof(TSource);

            MemberExpression member = propertyLambda.Body as MemberExpression;
            if (member == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyLambda.ToString()));

            PropertyInfo propInfo = member.Member as PropertyInfo;
            if (propInfo == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    propertyLambda.ToString()));

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
                throw new ArgumentException(string.Format(
                    "Expresion '{0}' refers to a property that is not from type {1}.",
                    propertyLambda.ToString(),
                    type));

            return propInfo;
        }

        public static bool HasAttribute(Type containerType, Type type)
        {
            throw new NotImplementedException();
        }

        public static bool HasAttribute(PropertyInfo propInfo, Type type)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Znajduje property dla podanego atrybutu. Jeżeli może być ich więcej należy użyć FindPropertiesByAttribute
        /// </summary>
        /// <param name="containerType"></param>
        /// <param name="attributeType"></param>
        /// <param name="inherit"></param>
        /// <returns></returns>
        public static PropertyInfo FindPropertyByAttribute(Type containerType, Type attributeType, bool inherit = true)
        {
            PropertyInfo[] props = containerType.GetProperties();
            foreach (PropertyInfo pi in props)
            {
                object[] objects = pi.GetCustomAttributes(attributeType, inherit);
                if (objects.Count() > 0)
                {
                    Debug.Assert(objects.Count() == 1);
                    return pi;
                }
            }

            return null;
        }

        public static PropertyInfo[] FindPropertiesByAttribute(Type containerType, Type attributeType, bool inherit = true)
        {
            throw new NotImplementedException();
        }


        public static bool IsXPCollection(object obj)
        {
            throw new NotImplementedException();
        }

        public static Type GetXpCollectionType(object obj)
        {
            throw new NotImplementedException();
        }



    }
}
