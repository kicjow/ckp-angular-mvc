﻿using CKP.FirstBlood.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CKP.FirstBlood.Module.Common
{
    public static class KursHelper
    {
        public static bool SprawdzCzyMaBycWidocznaFaktura(Aplikacja aplikacja)
        {
            if (aplikacja.Termin.Kurs.TypKursu == TypyKursów.Specjalizacyjne)
            {
                return false;
            }
            return true;
        }
    }
}
