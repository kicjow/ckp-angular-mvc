﻿using CKP.FirstBlood.Module.BusinessObjects;
using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CKP.FirstBlood.Module.DatabaseUpdate.FirstTime
{
    class BudujSlowniki
    {
        IObjectSpace _os;
        HashSet<string> userLists = new HashSet<string>();

        public BudujSlowniki(IObjectSpace os)
        {
            _os = os;
        }

        public void Process()
        {
            Slownik<Płeć>("plec_m", "Meżczyzna");
            Slownik<Płeć>("plec_k", "Kobieta");
            Slownik<RodzajDokumentu>("dok_do", "Dowód osobisty");
            Slownik<RodzajDokumentu>("dok_pas", "Paszport");
            Slownik<RodzajSpecjalnosci>("spc_lek", "Lekarskie");
            Slownik<RodzajSpecjalnosci>("spc_ldent", "Lekarsko-dentystyczne");
            _os.CommitChanges();

            ImportujSpecjalnosci();
        }

        private void ImportujSpecjalnosci()
        {
            using (StreamReader sr = new StreamReader("Import\\specjalizajeLek.txt", Encoding.UTF8))
            {
                var lek = DictBase.FromCode<RodzajSpecjalnosci>("spc_lek", _os);
                if (lek == null) throw new Exception();

                while(!sr.EndOfStream)
                {
                    var nazwa = sr.ReadLine().Trim();
                    var kod = "spec_lek_" + Regex.Replace(nazwa.ToLower(), @"ż|ć|ę|ł|ń|ó|ś|ź|ż|\s", "_");
                    var dict = Slownik<Specjalizacja>(kod, nazwa);
                    if (dict != null)
                    {
                        dict.RodzajSpecjalonosci = lek;
                    }
                }
            }
            using (StreamReader sr = new StreamReader("Import\\specjalizajeDent.txt", Encoding.UTF8))
            {
                var dent = DictBase.FromCode<RodzajSpecjalnosci>("spc_ldent", _os);
                if (dent == null) throw new Exception();

                while (!sr.EndOfStream)
                {
                    var nazwa = sr.ReadLine().Trim();
                    var kod = "spec_dent_" + Regex.Replace(nazwa.ToLower(), @"ż|ć|ę|ł|ń|ó|ś|ź|ż|\s", "_");
                    var dict = Slownik<Specjalizacja>(kod, nazwa);
                    if (dict != null)
                    {
                        dict.RodzajSpecjalonosci = dent;
                    }
                }
            }
        }

        private T Slownik<T>(string code, string nazwa) where T: DictBase
        {
            if (userLists.Contains(code))
            {
                throw new ArgumentException("Kod już użyty");
            }
            userLists.Add(code);

            if (_os.FindObject<T>(x => x.Kod == code) == null && _os.GetObjectsToSave(false).Cast<object>().Where(x => x is T).Cast<T>().FirstOrDefault(x => x.Kod == code) == null)
            {
                DictBase dict = _os.CreateObject<T>();
                dict.Nazwa = nazwa;
                dict.Kod = code;
                return dict as T;
            }
            return null;
        }
    }
}
