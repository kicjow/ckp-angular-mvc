﻿using CKP.FirstBlood.Module.BusinessObjects;
using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CKP.FirstBlood.Module.DatabaseUpdate.FirstTime
{
    class JednostkiImport
    {
        IObjectSpace _os;

        public JednostkiImport(IObjectSpace os)
        {
            _os = os;
        }

        public void Process()
        {
            using (StreamReader sr = new StreamReader("Import\\jednostki.txt", Encoding.UTF8))
            {
                while(!sr.EndOfStream)
                {
                    var line = sr.ReadLine().Split('\t');
                    if (line.Length != 2)
                        throw new Exception();
                    UtwozJednostke(line[0], line[1]);
                }
            }
        }

        private void UtwozJednostke(string nazwa, string kod)
        {
            if (_os.FindObject<JednostkaOrg>(x => x.Kod == kod) != null || _os.GetObjectsToSave(false).Cast<object>().Where(x => x is JednostkaOrg).Cast<JednostkaOrg>().FirstOrDefault(x => x.Kod == kod) != null)
                return;
            var jednostka = _os.CreateObject<JednostkaOrg>();
            jednostka.Kod = kod;
            jednostka.Nazwa = nazwa;
        }
    }
}
