﻿using CKP.FirstBlood.Module.BusinessObjects;
using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace CKP.FirstBlood.Module.DatabaseUpdate.FirstTime
{
    class KursyImport
    {
        IObjectSpace _os;

        public KursyImport(IObjectSpace os)
        {
            _os = os;
        }

        public void Process()
        {
            using (StreamReader sr = new StreamReader("Import\\kursy.txt", Encoding.UTF8))
            {
                while (!sr.EndOfStream)
                {
                    var data = sr.ReadLine().Split('\t');
                    if (data.Length != 17)
                        throw new Exception("Zła liczba kolumn");
                    UtworzKurs(data);
                }
            }
        }

        static readonly int[] kolumnyTerminow = new int[] { 5, 8, 11, 14 };
        private void UtworzKurs(string[] data)
        {
            int kod = int.Parse(data[0].Trim());
            var kurs = _os.FindObject<Kurs>(x => x.LpKursu == kod) ?? _os.GetObjectsToSave(false).Cast<object>().Where(x => x is Kurs).Cast<Kurs>().FirstOrDefault(x => x.LpKursu == kod);
            if (kurs != null)
                return;
            kurs = _os.CreateObject<Kurs>();
            kurs.LpKursu = kod;
            kurs.Jednostka = _os.FindObject<JednostkaOrg>(x => x.Kod == data[2].Trim());
            if (kurs.Jednostka == null)
            {
                throw new Exception("Jednostka zle zaimportowana");
            }
            kurs.Nazwa = data[1].Trim();
            var kierownik = _os.FindObject<KierownikKursu>(x => x.ImieNazwiskoTytul == data[3].Trim()) ?? _os.GetObjectsToSave(false).Cast<object>().Where(x => x is KierownikKursu).Cast<KierownikKursu>().FirstOrDefault(x => x.ImieNazwiskoTytul == data[3].Trim());
            if (kierownik == null)
            {
                kierownik = _os.CreateObject<KierownikKursu>();
                kierownik.ImieNazwiskoTytul = data[3].Trim();
            }
            kurs.Kierownik = kierownik;
            var specjalizacja = _os.GetObjects<Specjalizacja>(x => x.Nazwa == data[4].Trim()).ToList();
            if (specjalizacja.Count == 0)
            {
                throw new Exception("Nie znaleziono specjalizacji");
            }
            if (specjalizacja.Count > 1)
            {
                throw new Exception("Znaleziono więcej niż jedną pasującą specjalizację!");
            }
            kurs.Specjalizacja = specjalizacja[0];

            
            for (int i = 0; i < kolumnyTerminow.Length; i++)
            {
                if (!string.IsNullOrEmpty(data[kolumnyTerminow[i]].Trim()))
                {
                    TerminKursu termin = _os.CreateObject<TerminKursu>();
                    termin.Numer = i + 1;
                    termin.RejestracjaOtwarta = true;

                    if (!string.IsNullOrEmpty((data[kolumnyTerminow[i] + 1].Trim())))
                    {
                        termin.Początek = DateTime.ParseExact(data[kolumnyTerminow[i] + 1], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }

                    if (!string.IsNullOrEmpty((data[kolumnyTerminow[i] + 2].Trim())))
                    {
                        termin.Koniec = DateTime.ParseExact(data[kolumnyTerminow[i] + 2], "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    }
                    kurs.Terminy.Add(termin);
                }
            }
            
        }
    }
}
